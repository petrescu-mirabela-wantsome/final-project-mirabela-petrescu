# Wantsome - Final Project (Template)

## About

This is a **template** project for your final project at the end of Java course.

It's just a basic working IntelliJ Idea project, with a default folders structure,
and including some commonly used libraries (see in /lib, add others as needed)

## Project Structure
  - Folders:
    - `/src/main/java` folder is meant for main (non-test) sources of the app
    - `/src/test/java` folder is meant for test sources (JUnit..) of the app
  - Packages:
    - base package `wantsome.project` should be renamed to something appropriate to your project
    - package structure should be similar between your main and test folders
