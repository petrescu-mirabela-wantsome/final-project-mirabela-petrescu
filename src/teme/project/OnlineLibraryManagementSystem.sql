-- drop table user;
-- drop table history;
-- drop table item;
-- drop table customer;

-- create database wantsome;

create table user( 
id int primary key auto_increment, 
username varchar(100) not null unique,
password varchar(100) not null,
realname varchar(100) not null,
email varchar(100) not null unique,
usertype enum ('ADMIN', 'LIBRARIAN') not null);

create table item( 
id int primary key auto_increment, 
title varchar(100) not null,
author varchar(100) not null,
itemtype enum ('BOOK', 'MAGAZINE', 'ARTICLE', 'DIGITAL_STORAGE_MEDIA') not null,
itemstatus enum ('AVAILABLE', 'LENT', 'LOST', 'DESTROYED', 'UNAVAILABLE') not null);

create table customer(
id int primary key auto_increment,
name varchar(100) not null unique,
email varchar(100) not null unique,
phone varchar(100) not null);

create table history(
itemid int,
customerid int,
startdate date,
duedate date,
returndate date,
foreign key (itemid) references item (id),
foreign key (customerid) references customer(id),
primary key (itemid, customerid, startdate, duedate, returndate));

select * from user;
select * from item;
select * from history;
select * from customer;

delete from user where id >= 2;

delete from item where id >= 1;

delete from history where itemid >= 1;

UPDATE history SET returndate = '2017-07-31' WHERE itemid = 10 and startdate = '2019-07-16' and duedate = '2019-07-30';

INSERT INTO user(username, password, realname, email, usertype) VALUES("mira", "bela","Mirabela Petrescu", "mira@yahoo.com", "ADMIN");

-- SELECT h.itemid, i.title, i.author, i.itemtype, c.name, c.email, h.startdate, h.enddate FROM history h
-- JOIN customer c ON h.customerid = c.id
-- JOIN item i ON h.itemid = i.id
-- WHERE h.itemid = 1;

select * from customer where name like '%stel%';

-- select * from user where username = mira AND password = bela AND usertype = ADMIN;

select * from history where returndate != '1970-01-01' AND returndate > duedate;