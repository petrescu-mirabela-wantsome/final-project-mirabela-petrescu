package teme.project.customer;

import teme.project.general.DBConstants;
import teme.project.general.UiUtil;
import teme.project.user.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static teme.project.general.DBConstants.CustomerField.EMAIL;
import static teme.project.general.DBConstants.CustomerField.NAME;
import static teme.project.general.DBConstants.UserType.LIBRARIAN;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/AddCustomerServlet")
public class AddCustomerServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser hte type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        // get the new customer info written by the user in the browser
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, LIBRARIAN)) {
            if (UiUtil.isEmptyString(name) &&
                    UiUtil.isEmptyString(email) &&
                    UiUtil.isEmptyString(phone)) {

                CustomerDAO customerDAO = new CustomerDAO();
                CustomerDTO customerDTO = new CustomerDTO(name, email, phone);

                /*
                 * if the new user is not in database
                 * name and email shall be unique
                 */
                if (customerDAO.get(NAME, name).isEmpty() &&
                        customerDAO.get(EMAIL, email).isEmpty()) {

                    customerDAO.insert(customerDTO);      // write into user table of the data base the info written by the user in the browser

                    out.print("<p><strong><mark> !!!!!!!!! " +
                            customerDTO +
                            "<br> !!!!!!!!! Successfully Added.</mark></strong></p>");
                } else {
                    out.println("<p><strong><mark>!!!!! Duplicate entry. " +
                            "User already in database ==> " + customerDTO + "</p></strong></mark>");
                }

            /*
            The RequestDispatcher class enables your servlet to "call" another servlet from inside another servlet.
            The other servlet is called as if an HTTP request was sent to it by a browser.
            Link: http://tutorials.jenkov.com/java-servlets/requestdispatcher.html
            */
                // the information is correct and introduced into the database ==> current servlet is calling the AdminHome servlet
                request.getRequestDispatcher("CustomerHomeServlet").include(request, response);
            } else {
                // the current servlet is calling AddCustomer.html (because the information is not filled in correctly) ==> NOT introduced into the data base
                // the user has to introduce again the information
                out.print("<p><strong><mark>!!!!!! Please try again. The user properties are not filled correctly </p></strong></mark>");
                request.getRequestDispatcher("AddCustomer.html").include(request, response);
            }
        } else {
            out.print("</p></strong></mark>!!!! Sorry. Administrator already logged in. Please try again.</p></strong></mark>");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }


}
