package teme.project.customer;

import java.util.Comparator;

/*
LINK: https://howtodoinjava.com/sort/groupby-sort-multiple-comparators/
 */

class CustomerComparator {

    static class CustomerComparatorByName implements Comparator<CustomerDTO> {

        @Override
        public int compare(CustomerDTO o1, CustomerDTO o2) {
            return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
        }
    }


    static class CustomerComparatorByEmail implements Comparator<CustomerDTO> {

        @Override
        public int compare(CustomerDTO o1, CustomerDTO o2) {
            return o1.getEmail().toLowerCase().compareTo(o2.getEmail().toLowerCase());
        }
    }


    static class CustomerComparatorByPhone implements Comparator<CustomerDTO> {

        @Override
        public int compare(CustomerDTO o1, CustomerDTO o2) {

            return o1.getPhone().compareTo(o2.getPhone());
        }
    }

}
