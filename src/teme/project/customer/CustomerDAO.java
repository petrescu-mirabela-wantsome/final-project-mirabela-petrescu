package teme.project.customer;

import teme.project.general.AbstractDao;
import teme.project.general.DBConstants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAO extends AbstractDao<CustomerDTO, DBConstants.CustomerField> {

    //DB table related constants
    private static final String TABLE_NAME = "customer";
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String EMAIL = "email";
    private static final String PHONE = "phone";

    @Override
    protected List<CustomerDTO> executeSelect(Connection c) {
        String sql = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + ID;
        List<CustomerDTO> results = new ArrayList<>();
        try (PreparedStatement ps = c.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                extractCustomerFromResult(results, rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    private void extractCustomerFromResult(List<CustomerDTO> results, ResultSet rs) throws SQLException {
        results.add(new CustomerDTO(
                rs.getInt(ID),
                rs.getString(NAME),
                rs.getString(EMAIL),
                rs.getString(PHONE)));
    }

    @Override
    protected String authenticate(Connection c, String username, String password) {
        return null;
    }


    @Override
    protected List<CustomerDTO> executeSelect(Connection c, DBConstants.CustomerField option, String customerValue) {
        String sql = "";
        List<CustomerDTO> results = new ArrayList<>();
        switch (option) {
            case ID:
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = ?";
                break;
            case NAME:
                // LINK: https://alvinalexander.com/blog/post/jdbc/jdbc-preparedstatement-select-like
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE UPPER(" + NAME + ") LIKE ?";
                // going to do a search using "upper" and add the specific criteria
                customerValue = "%" + customerValue.toUpperCase() + "%";
                break;
            case EMAIL:
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + EMAIL + " = ?";
                break;
            case PHONE:
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + PHONE + " = ?";
                break;
            default:
                System.out.println("Item - no option");
                break;
        }

        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setString(1, customerValue);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                extractCustomerFromResult(results, rs);
            }
            return results;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected boolean executeSelect(Connection c, CustomerDTO userObject) {
        return false;
    }

    @Override
    protected void executeInsert(Connection c, CustomerDTO object) {
        String sql = "INSERT INTO " + TABLE_NAME + " ( " + NAME + ", " + EMAIL + ", " + PHONE + ") VALUES(?,?,?)";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setString(1, object.getName());
            ps.setString(2, object.getEmail());
            ps.setString(3, object.getPhone());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void executeUpdate(Connection c, CustomerDTO object) {
        String sql = "UPDATE " + TABLE_NAME +
                " SET " + NAME + " = ?, " + EMAIL + " = ?, " + PHONE + " = ? WHERE " + ID + " = ?";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setString(1, object.getName());
            ps.setString(2, object.getEmail());
            ps.setString(3, object.getPhone());
            ps.setInt(4, object.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void executeDelete(Connection c, CustomerDTO object) {
        String sql = "DELETE FROM " + TABLE_NAME + " WHERE " + ID + " = ?";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setInt(1, object.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
