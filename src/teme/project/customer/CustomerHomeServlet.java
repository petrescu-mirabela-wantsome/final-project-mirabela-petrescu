package teme.project.customer;

import teme.project.general.DBConstants;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static teme.project.general.DBConstants.UserType.LIBRARIAN;

/*
This Servlet shall update the database with the librarian data written by the user in the browser
(administrator has rights to introduce a librarian)
 */

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */

@WebServlet("/CustomerHomeServlet")
public class CustomerHomeServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * SEARCH CUSTOMER BY NAME
     * https://www.w3schools.com/howto/howto_js_filter_table.asp
     */
    private static void searchCostumerByName(PrintWriter out, List<CustomerDTO> customerDTOList) {
        // Link: https://www.quora.com/How-do-I-get-a-value-from-a-drop-down-list-in-JSP
        out.println("<br><strong><mark>Search customer by name</strong></mark><form action=\"SearchCustomerServlet\" method=\"POST\">" +

                "<table class='table table-bordered table-striped'>" +
                "<tr><td><label for=\"customername1\">Customer Name Account</label></td>" +
                "<td><input type=\"text\" class=\"form-control\" name=\"customername\" id=\"customername1\" placeholder=\"Customer Account\"/>" +
                "</td></tr></table>" +

                "<button type='submit' class='btn btn-primary'>Search</button>" +
                "<button type='reset' class='btn btn-primary'>Reset</button></form><br><br>");
    }

    /**
     * CUSTOMER TABLE - SORTED BY THE OPTIONS SELECTED IN THE BROWSER
     */
    private static void customerViewComparator(PrintWriter out) {
        out.print("<strong><mark>View Customers (persons who lent the items library) (database information)</strong></mark><br>");

        // LINK: https://www.codejava.net/java-ee/servlet/handling-html-form-data-with-java-servlet
        // LINK: https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_select_multiple

        // comparator - will contain all the selected checkboxes from the browser => it is an array
        out.print(" <form action='CustomerComparatorTableServlet' method='POST' style='width:300px'>" +
                "   Sort the customers<br>" +

                "   <input type=\"checkbox\" name=\"comparator\" value=\"customerCompByName\" > Sort the customers by name<br>" +
                "   <input type=\"checkbox\" name=\"comparator\" value=\"customerCompByEmail\" > Sort the customers by email<br>" +
                "   <input type=\"checkbox\" name=\"comparator\" value=\"customerCompByPhone\" > Sort the customers by phone<br>" +

                "   <button type=\"submit\" class=\"btn btn-primary\">View Customers</button>" +
                "   <button type='reset' class='btn btn-primary'>Reset the sort</button></form></body>");
    }

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser the type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, LIBRARIAN)) {

            /*
             * SESSION USER INFORMATION
             * LOGOUT BUTTON
             * BACK BUTTON
             */
            out.print(UiUtil.servletSessionInfo(session) +

                    "<h2>Customer View</h2>" +

                    "<a href='LogoutUserServlet' class='btn btn-primary' role='button'>Logout</a>" +

                    "<br><br><a href='LibrarianHomeServlet' class='btn btn-primary' role='button'>Back (Librarian Home view)</a>");

            /*
             * ADD NEW CUSTOMER
             * - link to a HTML template form
             */
            out.print("<br><br><a href='AddCustomer.html' class='btn btn-primary' role='button'>Add New Customer</a><br><br>");


            /*
             * SEARCH CUSTOMER BY NAME
             */
            searchCostumerByName(out, new CustomerDAO().get());

            /*
             * VIEW THE CUSTOMERS INTO A TABLE
             */
            customerViewComparator(out);

        } else {
            out.print("!!!! Sorry. Administrator already logged in. Please try again");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}
