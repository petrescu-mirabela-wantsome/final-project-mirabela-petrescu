package teme.project.customer;

import teme.project.general.DBConstants;
import teme.project.history.HistoryDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static teme.project.general.DBConstants.CustomerField.ID;
import static teme.project.general.DBConstants.HistoryField.CUSTOMER_ID;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/DeleteCustomerServlet")
public class DeleteCustomerServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser hte type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        String id = request.getParameter("id");   // get the customer id, customer value to be deleted, selected from the browser
        CustomerDAO customerDAO = new CustomerDAO();

        /*
        always there has to be at least one administrator user.
         */
        if (customerDAO.get().size() > 1) {
            CustomerDTO customerDTO = customerDAO.get(ID, id).get(0);

            // check if the customer to be deleted lent an library item
            if (new HistoryDAO().get(CUSTOMER_ID, id).size() == 0) {
                // delete the id from the database, customer table has unique ids
                customerDAO.delete(customerDTO);
                out.print("<p><strong><mark> !!!!!!!!! " +
                        customerDTO +
                        "<br> !!!!!!!!! Deleted from the database.</mark></strong></p>");
                request.getRequestDispatcher("CustomerComparatorTableServlet").include(request, response);
            } else {
                out.print("<p><strong><mark> !!!!!!!!! " +
                        customerDTO +
                        "<br> !!!!!!!!! Can not be deleted since he lent an item.</mark></strong></p>");
                request.getRequestDispatcher("CustomerComparatorTableServlet").include(request, response);
            }

        } else {

        /*
        The RequestDispatcher class enables your servlet to "call" another servlet from inside another servlet.
        The other servlet is called as if an HTTP request was sent to it by a browser.
        Link: http://tutorials.jenkov.com/java-servlets/requestdispatcher.html
        */
            // the current servlet is not changed, because there is no information in the database
            out.print("<p><strong><mark> !!!!!!!!! No customer in the database.</mark></strong></p>");
            request.getRequestDispatcher("CustomerHomeServlet").include(request, response);
        }
    }
}
