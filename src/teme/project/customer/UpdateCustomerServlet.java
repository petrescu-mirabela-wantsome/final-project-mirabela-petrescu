package teme.project.customer;

import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static teme.project.general.DBConstants.CustomerField.*;
import static teme.project.general.DBConstants.UserType.LIBRARIAN;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/UpdateCustomerServlet")
public class UpdateCustomerServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser the type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        /*
         * // http://tutorials.jenkov.com/java-servlets/httpsession.html
         * The HttpSession object represents a user session.
         * A user session contains information about the user across multiple HTTP requests.
         */
        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, LIBRARIAN)) {

            String userName = request.getParameter("name");
            String email = request.getParameter("email");
            String phone = request.getParameter("phone");
            String id = request.getParameter("id");   // get the id value to be deleted selected from the browser

            if (UiUtil.isEmptyString(userName) &&
                    UiUtil.isEmptyString(email) &&
                    UiUtil.isEmptyString(phone)) {

                CustomerDAO customerDAO = new CustomerDAO();
                /*
                 * ITEM table options (select by):
                 * -1 = no option
                 * 1 = customer id
                 * 2 = customer name
                 * 3 = customer email
                 * 4 = customer phone
                 */
                CustomerDTO customerDTOItem = customerDAO.get(ID, id).get(0);    // the customer id is unique in the sql table

                customerDTOItem.setName(userName);
                customerDTOItem.setEmail(email);
                customerDTOItem.setPhone(phone);
                customerDAO.update(customerDTOItem);

                /*
                 * if the updated user is not already present in data base, OR
                 * if the username is not already present in database
                 */
                if (customerDAO.get(NAME, userName).stream().anyMatch(s -> s.getId() != customerDTOItem.getId()) ||
                        customerDAO.get(EMAIL, email).stream().anyMatch(s -> s.getId() != customerDTOItem.getId())) {

                    customerDAO.update(customerDTOItem);

                    out.println("<p><strong><mark>!!!!! Duplicate entry. User already in database ==> " +
                            customerDTOItem + "</p></strong></mark>");
                    // write into user table of the data base the info written by the user in the browser
                } else {
                    out.print("<p><strong><mark> !!!!!!!!! " + customerDTOItem +
                            "<br> !!!!!!!!! Successfully Updated.</mark></strong></p>");
                }

                request.getRequestDispatcher("CustomerComparatorTableServlet").include(request, response);

            } else {
                // the current servlet is calling AddUserFormServlet (because the information is not filled in correctly) ==> NOT introduced into the data base
                // the user has to introduce again the information into the browser
                out.print("<br><p><strong><mark>!!!!!! Please try again. The user properties are not filled correctly </p></strong></mark>" +
                        "   <td><a href='UpdateCustomerFormServlet?id=" + id + "'>Try again to introduce</a></td>");
            }
        } else {
            out.print("</p></strong></mark>!!!! Sorry. Administrator already logged in. Please try again.</p></strong></mark>");

            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}
