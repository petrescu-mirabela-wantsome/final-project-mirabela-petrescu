package teme.project.general;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public abstract class AbstractDao<T, E> {

    public List<T> get() {
        try (Connection connection = getConnection()) {
            return executeSelect(connection);
        } catch (SQLException e) {
            System.err.println("Error while get of all objects");
            //e.printStackTrace();
            return null;
        }
    }

    // Authentication
    public String get(String username, String password) {
        try (Connection connection = getConnection()) {
            return authenticate(connection, username, password);
        } catch (SQLException e) {
            System.err.println("Error while get object with username " + username + " and password " + password);
            //e.printStackTrace();
            return null;
        }
    }

    // Check if an item already exist in database
    public boolean get(T object) {
        try (Connection connection = getConnection()) {
            return !executeSelect(connection, object);
        } catch (SQLException e) {
            System.err.println("Error while get object " + object);
            //e.printStackTrace();
            return true;
        }
    }

    public List<T> get(E option, String parameterValue) {
        try (Connection connection = getConnection()) {
            return executeSelect(connection, option, parameterValue);
        } catch (SQLException e) {
            System.err.println("Error while get of object with parameter value " + parameterValue);
            //e.printStackTrace();
            return null;
        }
    }

    public void insert(T object) {
        try (Connection connection = getConnection()) {
            executeInsert(connection, object);
        } catch (SQLException e) {
            System.err.println("Error while inserting object " + object);
            //e.printStackTrace();
        }
    }

    public void update(T object) {
        try (Connection connection = getConnection()) {
            executeUpdate(connection, object);
        } catch (SQLException e) {
            System.err.println("Error while updating object " + object);
            //e.printStackTrace();
        }
    }

    public void delete(T object) {
        try (Connection connection = getConnection()) {
            executeDelete(connection, object);
        } catch (SQLException e) {
            System.err.println("Error while deleting object " + object);
            //e.printStackTrace();
        }
    }

    protected abstract List<T> executeSelect(Connection c);

    protected abstract String authenticate(Connection c, String username, String password);

    protected abstract List<T> executeSelect(Connection c, E option, String parameterValue);

    protected abstract boolean executeSelect(Connection c, T userObject);

    protected abstract void executeInsert(Connection c, T object);

    protected abstract void executeUpdate(Connection c, T object);

    protected abstract void executeDelete(Connection c, T object);

    private Connection getConnection() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
//        return DriverManager.getConnection(
//                "jdbc:mysql://159.69.118.199/wantsome_java6_mirabela",
//                "wantsome_qa", "22ehf!E0");

        return DriverManager.getConnection(ReadPropertiesFile.readUrlFromFile(ReadPropertiesFile.loadConfigFromFile()),
                ReadPropertiesFile.readUserNameFromFile(ReadPropertiesFile.loadConfigFromFile()),
                ReadPropertiesFile.readPasswordFromFile(ReadPropertiesFile.loadConfigFromFile()));
    }
}
