package teme.project.general;

public class DBConstants {

    public static final long DATE_MIN_01 = -7200000;      //long value of new Date(0) = 1970-01-01
    public static final long DATE_MIN_02 = -10800000;     //long value of new Date(0) = 1969-12-31

    public static final int MAX_BOOKS_ISSUED_TO_A_USER = 5;

    public enum UserType {
        ADMIN,
        LIBRARIAN,
    }

    public enum ItemType {
        BOOK,
        MAGAZINE,
        ARTICLE,
        DIGITAL_STORAGE_MEDIA
    }

    public enum ItemStatus {
        AVAILABLE,
        LENT,
        LOST,
        DESTROYED,
        UNAVAILABLE
    }

    /**
     * Search fields of item table (these are the table columns)
     */
    public enum ItemField {
        ID,
        TITLE,
        AUTHOR,
        ITEM_TYPE,
        ITEM_STATUS
    }

    /**
     * Search field of user table (these are the table columns)
     */
    public enum UserField {
        ID,
        USERNAME,
        PASSWORD,
        REAL_NAME,
        EMAIL,
        USER_TYPE
    }

    /**
     * Search field of customer table (these are the table columns)
     */
    public enum CustomerField {
        ID,
        NAME,
        EMAIL,
        PHONE
    }

    /**
     * Search field of history table (these are the table columns)
     */
    public enum HistoryField {
        ITEM_ID,
        CUSTOMER_ID,
        START_DATE,
        DUE_DATE,
        RETURN_DATE,
        OVERDUE
    }

}
