package teme.project.general;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateFormatter {

    /*
    transform into sql date
     */
    public static java.sql.Date getSqlDate(String strDate) {
        java.sql.Date sqlDate = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date utilDate = format.parse(strDate);
            sqlDate = new java.sql.Date(utilDate.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sqlDate;
    }
}
