/*
 * Copyright (c) 2014 Oracle and/or its affiliates. All rights reserved.
 * <p>
 * You may not modify, use, reproduce, or distribute this software except in
 * compliance with  the terms of the License at:
 * https://github.com/javaee/tutorial-examples/LICENSE.txt
 */

package teme.project.general;

/*
 * LINK: https://docs.oracle.com/javaee/6/tutorial/doc/glraq.html
 * LINK: https://github.com/javaee/tutorial-examples/blob/master/web/servlet/fileupload/src/main/java/javaeetutorial/fileupload/FileUploadServlet.java
 */

import teme.project.item.ItemDAO;
import teme.project.item.ItemDTO;
import teme.project.user.UserDAO;
import teme.project.user.UserDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static teme.project.general.DBConstants.UserField.EMAIL;
import static teme.project.general.DBConstants.UserField.USERNAME;
import static teme.project.general.DBConstants.UserType.ADMIN;
import static teme.project.general.DBConstants.UserType.LIBRARIAN;

/*
 * File upload servlet example
 */
@WebServlet("/FileUploadServlet")

/*     >>>>>  @MultipartConfig  >>>>>
 * LINK: https://www.codejava.net/java-ee/servlet/multipartconfig-annotation-examples
 * The @MultipartConfig annotation is used to annotate a servlet class in order to handle multipart/form-data requests and configure various upload settings.
 * When a servlet is annotated by this annotation, we can access all parts via the methods getParts() and an individual part via the method getPart(name)
 * of the HttpServletRequest object, and write the upload file to disk via the method write(fileName) of the Part object.
 *
 * Use this annotation if you want to handle file upload through servlet
 */
@MultipartConfig
public class FileUploadServlet extends HttpServlet {

    /*
     * READ THE DATA FROM THE CSV FILE, IMPORTED IN THE BROWSER
     * WRITE THE DATA INTO AN ARRAY LIST
     */
    private static List<UserDTO> readUserFromCsv(Part filePart, PrintWriter writer) {
        List<UserDTO> userDTOList = new ArrayList<>();
        // Link: https://www.codota.com/code/java/classes/javax.servlet.http.Part
        BufferedReader reader;
        try {
            /*
             * getInputStream
             * The servlet engine does not help you parse or interpret that data. You just get it raw.
             */
            reader = new BufferedReader(new InputStreamReader(filePart.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] csvLineArray = line.split(",");
                try {
                    userDTOList.add(new UserDTO(csvLineArray[0], csvLineArray[1], csvLineArray[2], csvLineArray[3], DBConstants.UserType.valueOf(csvLineArray[4])));

                } catch (NumberFormatException e1) {
                    writer.println("<p><strong><mark>!!!!! Wrong type => " + Arrays.toString(csvLineArray) + "</p></strong></mark>");
                    System.out.println("Wrong type => " + Arrays.toString(csvLineArray));

                } catch (ArrayIndexOutOfBoundsException e2) {
                    writer.println("<p><strong><mark>!!!!! Less fields than expected => " + Arrays.toString(csvLineArray) + "</p></strong></mark>");
                    System.out.println("Less fields than expected => " + Arrays.toString(csvLineArray));

                } catch (IllegalArgumentException e3) {
                    writer.println("<p><strong><mark>!!!!! Wrong type => " + Arrays.toString(csvLineArray) + "</p></strong></mark>");
                    System.out.println("Wrong type => " + Arrays.toString(csvLineArray));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userDTOList;
    }

    /*
     * READ THE DATA FROM THE CSV FILE, IMPORTED IN THE BROWSER
     * WRITE THE DATA INTO AN ARRAY LIST
     */
    private static List<ItemDTO> readItemFromCsv(Part filePart, PrintWriter writer) {
        // Link: https://www.codota.com/code/java/classes/javax.servlet.http.Part
        List<ItemDTO> itemDTOList = new ArrayList<>();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(filePart.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] csvLineArray = line.split(",");
                try {
                    itemDTOList.add(new ItemDTO(csvLineArray[0], csvLineArray[1], DBConstants.ItemType.valueOf(csvLineArray[2]), DBConstants.ItemStatus.valueOf(csvLineArray[3])));
                } catch (NumberFormatException e1) {
                    System.out.println("Wrong type => " + Arrays.toString(csvLineArray));
                    writer.println("<p><strong><mark>!!!!! Wrong type => " + Arrays.toString(csvLineArray) + "</p></strong></mark>");
                } catch (ArrayIndexOutOfBoundsException e2) {
                    writer.println("<p><strong><mark>!!!!! Less fields than expected => " + Arrays.toString(csvLineArray) + "</p></strong></mark>");
                    System.out.println("Less fields than expected => " + Arrays.toString(csvLineArray));
                } catch (IllegalArgumentException e3) {
                    writer.println("<p><strong><mark>!!!!! Wrong type => " + Arrays.toString(csvLineArray) + "</p></strong></mark>");
                    System.out.println("Wrong type => " + Arrays.toString(csvLineArray));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return itemDTOList;
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        processRequest(request, response);
    }

    private String getFileName(final Part part) {
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        /*
         * // http://tutorials.jenkov.com/java-servlets/httpsession.html
         * The HttpSession object represents a user session.
         * A user session contains information about the user across multiple HTTP requests.
         */
        HttpSession session = request.getSession();

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        final PrintWriter writer = response.getWriter();

        // Create path components to save the file
        final String path = request.getParameter("destination");
        final Part filePart = request.getPart("file");      // Gets the Part with the given name.

        //final String fileName = getFileName(filePart);
        final String fileName = new File(Objects.requireNonNull(getFileName(filePart))).getName();

        try (OutputStream out = new FileOutputStream(new File(path + File.separator + fileName)); InputStream filecontent = filePart.getInputStream()) {

            int read;
            final byte[] bytes = new byte[1024];

//           //NOT USED - BUT KEPT FOR THE IMPLEMENTATION
//            /*
//            EXPORT THE INFORMATION FROM THE DATABASE INTO A CSV FILE
//             */
//            writeUserFromDatabase(new FileOutputStream(new File(path + File.separator + "Export"
//                    + fileName)));

            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            writer.println("<p><strong><mark>!!!!! New file " + fileName + " created at " + path + "</p></strong></mark>");


            /*
             * READ INFO FROM CSV UPLOADED BY THE USER - administrator account
             */
            if (UiUtil.checkServletSession(session, ADMIN)) {

                /*
                 * ADD THE USER DATA FROM THE CSV (UPLOADED IN THE BROWSER) IN THE DATABASE
                 */
                addUserInDataBaseFromCsv(writer, filePart);

                request.getRequestDispatcher("AdminHomeServlet").include(request, response);
            }
            /*
             * READ INFO FROM CSV UPLOADED BY THE USER - librarian account
             */
            else if (UiUtil.checkServletSession(session, LIBRARIAN)) {

                /*
                 * ADD THE ITEM DATA FROM THE CSV (UPLOADED IN THE BROWSER) IN THE DATABASE
                 */
                addItemInDataBaseFromCsv(writer, filePart);

                request.getRequestDispatcher("ItemHomeServlet").include(request, response);
            }
        } catch (FileNotFoundException fne) {
            writer.print("<p><strong><mark>!!!!! You either did not specify a file to upload or are " +
                    "   trying to upload a file to a protected or nonexistent " +
                    "   location.</p></strong></mark>" +
                    "   <p><strong><mark>!!!!! ERROR: " + fne.getMessage() + "</p></strong></mark>");

            /*
             * In case of exception and admin account
             */
            if (UiUtil.checkServletSession(session, ADMIN)) {
                request.getRequestDispatcher("AdminHomeServlet").include(request, response);
            }
            /*
             * In case of exception and librarian account
             */
            else if (UiUtil.checkServletSession(session, LIBRARIAN)) {
                request.getRequestDispatcher("ItemHomeServlet").include(request, response);
            }
        }
    }

    private void addItemInDataBaseFromCsv(PrintWriter writer, Part filePart) {
        /*
         * ADD THE ITEM DATA FROM THE CSV (UPLOADED IN THE BROWSER) IN THE DATABASE
         */
        ItemDAO itemDAO = new ItemDAO();
        for (ItemDTO itemDTO : readItemFromCsv(filePart, writer)) {
            writer.println("<p><strong><mark>!!!!!! User " + itemDTO +
                    "<br>!!!!!! Successfully introduced in the database" + "</p></strong></mark>");
            itemDAO.insert(itemDTO);      // write into user table of the data base the info written by the user in the browser
        }
    }


    /**
     * ADD THE USER DATA FROM THE CSV (UPLOADED IN THE BROWSER) IN THE DATABASE
     */
    private void addUserInDataBaseFromCsv(PrintWriter writer, Part filePart) {

        UserDAO userDAO = new UserDAO();
        for (UserDTO userDTO : readUserFromCsv(filePart, writer)) {


            /*
             * if the updated user is not already present in data base, OR
             * if the username is not already present in database
             */
            if (userDAO.get(USERNAME, userDTO.getUsername()).stream().anyMatch(s -> s.getId() != userDTO.getId()) ||
                    userDAO.get(EMAIL, userDTO.getEmail()).stream().anyMatch(s -> s.getId() != userDTO.getId())) {

                userDAO.update(userDTO);

                writer.println("<p><strong><mark>!!!!! Duplicate entry. User already in database ==> " +
                        userDTO + "</p></strong></mark>");
                // write into user table of the data base the info written by the user in the browser
            } else {
                writer.print("<p><strong><mark> !!!!!!!!! " + userDTO +
                        "<br> !!!!!!!!! Successfully Updated.</mark></strong></p>");
            }

//            /*
//             * if the new user is not in database
//             * username and email are unique in database
//             */
//            if (userDAO.get(USERNAME, userDTO.getUsername()).isEmpty() &&
//                    userDAO.get(EMAIL, userDTO.getEmail()).isEmpty() &&
//                    userDAO.get(userDTO)) {
//                writer.println("<p><strong><mark>!!!!!! User " + userDTO +
//                        "<br>!!!!!! Successfully introduced in the database" + "</p></strong></mark>");
//                userDAO.insert(userDTO);      // write into user table of the data base the info written by the user in the browser
//            }
//            /*
//             * if the new user read from csv file already exist in database
//             */
//            else {
//                writer.println("<p><strong><mark>!!!!! Duplicate entry. User already in database ==> " + userDTO + "</p></strong></mark>");
//            }
        }
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet that uploads files to a user-defined destination";
    }


    // NOT USED - BUT KEPT FOR THE IMPLEMENTATION
//    /**
//     * READ THE DATA FROM THE CSV FILE, IMPORTED IN THE BROWSER
//     * WRITE THE DATA INTO AN ARRAY LIST
//     */
//    private static void writeUserFromDatabase(OutputStream out, List<UserDTO> userDatabaseDTOList) throws IOException {
//        // Link: https://www.codota.com/code/java/classes/javax.servlet.http.Part
//        StringBuilder stringUserFromDatabase = new StringBuilder();
//        for (UserDTO userDTO : userDatabaseDTOList) {
//            stringUserFromDatabase.append(userDTO.getId()).append(",")
//                    .append(userDTO.getUsername()).append(",")
//                    .append(userDTO.getPassword()).append(",")
//                    .append(userDTO.getRealName()).append(",")
//                    .append(userDTO.getEmail()).append(",")
//                    .append(userDTO.getUsertype()).append("\n");
//        }
//
//        writeToFileInfo(stringUserFromDatabase.toString(), out);
//    }
//
//    private static void writeToFileInfo(String text, OutputStream out) throws IOException {
//        char[] cArray = text.toCharArray();
//        for (char textChar : cArray) {
//            out.write(textChar);
//        }
//    }
}