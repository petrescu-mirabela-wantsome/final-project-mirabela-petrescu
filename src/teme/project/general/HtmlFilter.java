package teme.project.general;

// Link: https://www.ntu.edu.sg/home/ehchua/programming/java/JavaServlets.html

/*
 * Replace the special HTML characters (>, <, &, ") with the HTML escape sequences in the input strings,
 * before we echo them back to the client via out.println().
 * This step is necessary to prevent the so-called command-injection attack, where user enters a script into the text field.
 * The replacement is done via a static helper method htmlFilter().
 *
 * Rule of thumb: Any text string taken from the client and echoing back via out.println() needs to be filtered!
 */

class HtmlFilter {

    // Filter the string for special HTML characters to prevent
    // command injection attack

    private static String htmlFilter(String message) {
        if (message == null) return null;
        int len = message.length();
        StringBuffer result = new StringBuffer(len + 20);
        char aChar;

        for (int i = 0; i < len; ++i) {
            aChar = message.charAt(i);
            switch (aChar) {
                case '<':
                    result.append("&lt;");
                    break;
                case '>':
                    result.append("&gt;");
                    break;
                case '&':
                    result.append("&amp;");
                    break;
                case '"':
                    result.append("&quot;");
                    break;
                default:
                    result.append(aChar);
            }
        }
        return (result.toString());
    }


    /**
     * check if the
     *
     * @param value - string value read from the browser (introduced by the user in the browser)
     * @return - false in case the string value is not valid
     */
    public static boolean isHtmlFilter(String value) {
        return (value == null || (HtmlFilter.htmlFilter(value.trim())).length() == 0);
    }
}
