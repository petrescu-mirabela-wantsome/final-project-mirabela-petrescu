package teme.project.general;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadPropertiesFile {

    //define some constants (related to config file)
    // Home computer path

    //private final static String CONFIG_FILENAME = "D:\\mirabela_work\\proiect_mirabela\\src\\project.properties";
    // Work computer path

    private final static String CONFIG_FILENAME = "D:\\Trainings\\Wantsome\\proiect_mirabela\\src\\project.properties";
    private final static String CONFIG_KEY_URL = "url";
    private final static String CONFIG_KEY_USERNAME = "user";
    private final static String CONFIG_KEY_PASSWORD = "password";
    private final static String CONFIG_KEY_USER_CSV_PATH = "usercsvpath";
    private final static String CONFIG_KEY_UPLOADED_FILES_PATH = "uploadedFilesPath";

    public static Properties loadConfigFromFile() {
        Properties config = new Properties();
        try (InputStream in = new FileInputStream(CONFIG_FILENAME)) {
            config.load(in);
            //System.out.println("Success loading data from config file: " + config);
        } catch (IOException io) {
            //System.err.println("Config file missing.");
        }
        return config;
    }

    public static String readUrlFromFile(Properties config) {
        String url = "";//if data was loaded from config file and we found our expected key for username
        if (config.containsKey(CONFIG_KEY_URL)) {
            url = config.getProperty(CONFIG_KEY_URL); //then get the value for that key from the config, and use it as username
        }
        return url;
    }

    public static String readUserNameFromFile(Properties config) {
        String userName = "";//if data was loaded from config file and we found our expected key for username
        if (config.containsKey(CONFIG_KEY_USERNAME)) {
            userName = config.getProperty(CONFIG_KEY_USERNAME); //then get the value for that key from the config, and use it as username
        }
        return userName;
    }

    public static String readPasswordFromFile(Properties config) {
        String password = "";//if data was loaded from config file and we found our expected key for username
        if (config.containsKey(CONFIG_KEY_PASSWORD)) {
            password = config.getProperty(CONFIG_KEY_PASSWORD); //then get the value for that key from the config, and use it as username
        }
        return password;
    }

    public static String readUserCsvPath(Properties config) {
        String userCvasPath = "";
        if (config.containsKey(CONFIG_KEY_USER_CSV_PATH)) {
            userCvasPath = config.getProperty(CONFIG_KEY_USER_CSV_PATH);
        }
        return userCvasPath;
    }

    public static String readUploadedFilesPath(Properties config) {
        String userCvasPath = "";
        if (config.containsKey(CONFIG_KEY_UPLOADED_FILES_PATH)) {
            userCvasPath = config.getProperty(CONFIG_KEY_UPLOADED_FILES_PATH);
        }
        return userCvasPath;
    }
}
