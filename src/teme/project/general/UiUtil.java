package teme.project.general;

import teme.project.customer.CustomerDTO;
import teme.project.history.HistoryDTO;
import teme.project.item.ItemDTO;
import teme.project.user.UserDTO;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Utility methods for user interface
 */

public class UiUtil {

    /**
     * Check if a string value is empty - remove white spaces
     *
     * @param value - string
     * @return true if the parameter value is space or nothing
     */
    public static boolean isEmptyString(String value) {
        return !value.replaceAll("\\s+", "").isEmpty();
    }


    /**
     * CREATE A DROP DOWN LIST OF AN ENUM (generic type) (<option></>)
     * The item information is taken from the database
     */
    public static <T> void createEnumDropDownList(PrintWriter out, T[] enumValues, T defaultItemValue) {
        for (T itemEnum : enumValues) {
            if (itemEnum.equals(defaultItemValue)) {
                // Link: https://www.w3schools.com/tags/att_option_selected.asp - the value from database will be selected from the list
                // selected --> means that by default will be selected the item type which is currently set in the database
                out.println("<option selected>" + itemEnum + "</option>");
            } else {
                out.println("<option>" + itemEnum + "</option>");
            }
        }
        out.print("</select></td></tr>");
    }


    /**
     * RETURN A STRING FROM AN OBJECT
     * - each object field separated by ","
     * - each object list separated by "\n"
     */
    public static <T> String getStringFromObject(List<T> obj, Function<T, String> mapToString) {
        return obj.stream()
                .map(i -> mapToString.apply(i))
                .collect(Collectors.joining("\n"));
    }


    /**
     * WRITE STRING INFO TO A STREAM
     */
    public static void writeToFileInfo(String text, OutputStream out) throws IOException {
        char[] cArray = text.toCharArray();
        for (char textChar : cArray) {
            out.write(textChar);
        }
    }

    /**
     * HTML COMMON PAGE INFO (the information will appear in the browser)
     */
    public static String servletSessionInfo(HttpSession session) {
        return
                "   <br>User Name = <strong>" + session.getAttribute("userNameSession") + "</strong>" +
                        "   <br>User Type = <strong>" + session.getAttribute("userSession") + "</strong>" +
                        "   <br>User login time is <strong>" + new Date(session.getCreationTime()) + "</strong>" +
                        "   <br>User login id is <strong>" + session.getId() + "</strong>" +
                        "   <br>User login is new <strong>" + session.isNew() + "</strong>";
    }


    /**
     * Get the days since the item it was lent until it was returned
     * // LINK: https://www.baeldung.com/java-date-difference
     *
     * @return days since lent date until return date = return date - due date
     */
    public static Long getLentDays(HistoryDTO historyDTOIdItem) {
        long diffInMilliseconds = (historyDTOIdItem.getDueDate().getTime() - historyDTOIdItem.getReturnDate().getTime());

        return TimeUnit.DAYS.convert(diffInMilliseconds, TimeUnit.MILLISECONDS);
    }


    /**
     * CHECK THE USER SESSION - user type and user name account
     */
    public static boolean checkServletSession(HttpSession session, DBConstants.UserType userSession) {
        return session.getAttribute("userSession") != null &&
                session.getAttribute("userSession").equals(userSession); //&&
        // this is a new session - only one user is allowed to be logged in under one session
        //session.isNew();
    }


    /**
     * CUSTOMER TABLE INFORMATION FROM DATABASE (HTML FORMAT)
     * <tr>	Defines a row in a table
     * <td>	Defines a cell in a table
     */
    public static void customerViewTable(PrintWriter out, List<CustomerDTO> customerDTOList) {

        out.print("<br><h3>Customers (database information)</h3>" +
                "<br><table class='table table-bordered table-striped'>" +
                "<tr><th><ins></ins>Id</th>" +
                "<th><ins>Name</ins></th>" +
                "<th><ins>Email</ins></th>" +
                "<th><ins>Phone</ins></th>" +
                "<th><ins>Update</ins></th>" +
                "<th><ins>Delete</ins></th>" +
                "<th><ins>History of loans</ins></th></tr>");

        if (customerDTOList.size() > 0) {

            for (CustomerDTO customerDTO : customerDTOList) {
                out.println("<tr><td align=\"center\">" + customerDTO.getId() +
                        "   </td><td align=\"center\">" + customerDTO.getName() +
                        "   </td><td align=\"center\">" + customerDTO.getEmail() +
                        "   </td><td align=\"center\">" + customerDTO.getPhone() +
                        "   </td>" +
                        "   <td><a href='UpdateCustomerFormServlet?id=" + customerDTO.getId() + "'>Update</a></td>" +
                        "   <td><a href='DeleteCustomerServlet?id=" + customerDTO.getId() + "'>Delete</a></td>" +
                        "   <td><a href='HistoryCustomerServlet?id=" + customerDTO.getId() + "'>Check History</a></td></tr>");
            }
            out.println("</table>");
        } else {
            out.println("</table>");
        }
    }


    /**
     * LIBRARY VIEW TABLE
     * <tr>	Defines a row in a table
     * <td>	Defines a cell in a table
     */
    public static void itemViewTable(PrintWriter out, List<ItemDTO> itemDTOList) {
        out.print("<br><strong><mark>View Library Items (database information)</strong></mark>" +
                "<table class='table table-bordered table-striped'>" +
                "<tr><th><ins>Id</ins></th>" +
                "<th><ins>Title</ins></th>" +
                "<th><ins>Author</ins></th>" +
                "<th><ins>Item_Type</ins></th>" +
                "<th><ins>Item_Status</ins></th>" +
                "<th><ins>Update</ins></th>" +
                "<th><ins>History of loans</ins></th></tr>");

        if (itemDTOList.size() > 0) {
            // write the information into the browser

            for (ItemDTO itemDTO : itemDTOList) {
                out.println("<tr><td align=\"center\">" + itemDTO.getId() +
                        "</td><td align=\"center\">" + itemDTO.getTitle() +
                        "</td><td align=\"center\">" + itemDTO.getAuthor() +
                        "</td><td align=\"center\">" + itemDTO.getItemType() +
                        "</td><td align=\"center\">" + itemDTO.getItemStatus() +
                        "</td>");
                out.println("<td><a href='UpdateItemFormServlet?id=" + itemDTO.getId() + "'>Update</a></td>");
                out.println("<td><a href='HistoryItemServlet?id=" + itemDTO.getId() + "'>Check History</a></td></tr>");
            }
            out.println("</table>");
        } else {
            out.println("</table>");
        }
    }


    /**
     * USER VIEW TABLE
     * <tr>	Defines a row in a table
     * <td>	Defines a cell in a table
     */
    public static void userViewTable(PrintWriter out, List<UserDTO> userDTOList) {

        out.print("<br><h3><strong><mark>View Users (database information)</strong></mark></h3>" +
                "<table class='table table-bordered table-striped'>" +
                "<tr><th><ins>Id</ins></th>" +
                "<th><ins>User_Name</ins></th>" +
                "<th><ins>Real_Name</ins></th>" +
                "<th><ins>Email</ins></th>" +
                "<th><ins>User_Type</ins></th>" +
                "<th><ins>Update</ins></th>" +
                "<th><ins>Delete</ins></th></tr>");

        if (userDTOList.size() > 0) {
            /*
            <tr>	Defines a row in a table
            <td>	Defines a cell in a table
             */
            for (UserDTO userDTOItem : userDTOList) {
                out.println("<tr><td align=\"center\">" + userDTOItem.getId() +
                        "</td><td align=\"center\">" + userDTOItem.getUsername() +
                        "</td><td align=\"center\">" + userDTOItem.getRealName() +
                        "</td><td align=\"center\">" + userDTOItem.getEmail() +
                        "</td><td align=\"center\">" + userDTOItem.getUserType() +
                        "</td>");
                out.print("</td>");

                out.println("<td><a href='UpdateUserFormServlet?id=" + userDTOItem.getId() + "'>Update</a></td>");
                out.println("<td><a href='DeleteUserServlet?id=" + userDTOItem.getId() + "'>Delete</a></td></tr>");
            }
            out.println("</table>");
        } else {
            out.println("</table>");
        }
    }


    /**
     * FIND THE ITEM RETURN DATE (ITEM and CUSTOMER HISTORY CASES)
     * - if the item is still lent, then the return date value appears as STILL LENT
     */
    public static void getHistoryReturnDate(PrintWriter out, HistoryDTO historyDTO, ItemDTO itemDTO) {
        // new Date(0) = 1970-01-01 used to specify that the item was lent and the return date is not set
        // new Date(0) = 1969-12-31 used to specify that the item was lent and the return date is not set
        if (historyDTO.getReturnDate().getTime() != DBConstants.DATE_MIN_01 &&
                historyDTO.getReturnDate().getTime() != DBConstants.DATE_MIN_02) {
            out.println("</td><td align=\"center\">" + historyDTO.getReturnDate() +
                    "   </td><td align=\"center\">" + UiUtil.getLentDays(historyDTO) +
                    "   </td></tr>");
        } else if (itemDTO.getItemStatus().equals(DBConstants.ItemStatus.LENT)) {
            out.println("</td><td align=\"center\">" + "STILL LENT" +
                    "   </td><td align=\"center\">" + "STILL LENT" +
                    "   </td></tr>");
        }
    }
}


