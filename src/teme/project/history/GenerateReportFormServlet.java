package teme.project.history;

import teme.project.general.DBConstants;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/GenerateReportFormServlet")
public class GenerateReportFormServlet extends HttpServlet {


    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser the type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        HttpSession session = request.getSession();

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        if (UiUtil.checkServletSession(session, DBConstants.UserType.LIBRARIAN)) {

            out.print("<!DOCTYPE html><html><head><title>Generate Report Form</title></head><body>" +
                    UiUtil.servletSessionInfo(session) +
                    "   <br><br><a href='LogoutUserServlet' class='btn btn-primary' role='button'>Logout</a><br><br>" +
                    "   <a href='ItemHomeServlet' class='btn btn-primary' role='button'>Back (Library Item Home view)</a><br><br>");

            // LINK: https://www.codejava.net/java-ee/servlet/handling-html-form-data-with-java-servlet
            // LINK: https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_select_multiple
            // LINK: https://ibytecode.com/blog/getting-checkbox-values-from-html-form-in-servlet/

            // report - will contain all the selected checkboxes from the browser => it is an array
            out.print(" <form action='GenerateReportServlet' method='post' style='width:300px'>" +
                    "   <h3>Generate Report</h3>" +
                    "   <input type=\"checkbox\" name=\"report\" value=\"itemByStatus\" > Select the items by status<br>" +
                    "   <label for='itemstatus1'>Item Status</label>" +
                    "   <td><select name=\"itemstatus\">");
            UiUtil.createEnumDropDownList(out, DBConstants.ItemStatus.values(), DBConstants.ItemStatus.values()[0]);

            out.print("<br><br><input type=\"checkbox\" name=\"report\" value=\"itemOverdue\" > The lent items that are overdue<br><br>" +
                    "   <input type=\"checkbox\" name=\"report\" value=\"itemByType\" > Select the items by type");

            out.println("<br><label for='itemtype1'>Item Type</label>" +
                    "<td><select name=\"itemtype\">");
            UiUtil.createEnumDropDownList(out, DBConstants.ItemType.values(), DBConstants.ItemType.values()[0]);

            out.print("<br><br><input type=\"checkbox\" name=\"report\" value=\"itemByAuthor\" > Select the item by author<br>" +
                    "   <label for='itemauthor1'>Item author</label>" +
                    "   <input type=\"text\" class=\"form-control\" name=\"itemauthor\" id=\"itemauthor1\" placeholder=\"Item author\"/><br><br>" +
                    "   <button type=\"submit\" class=\"btn btn-primary\">Generate</button>" +
                    "   <button type='reset' class='btn btn-primary'>Reset</button></form></body>");

        } else {
            out.print("!!!! Sorry. Administrator already logged in. Please try again");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}
