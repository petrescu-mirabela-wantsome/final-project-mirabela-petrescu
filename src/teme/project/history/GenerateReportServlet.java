package teme.project.history;

import com.itextpdf.text.DocumentException;
import teme.project.customer.CustomerDAO;
import teme.project.customer.CustomerDTO;
import teme.project.general.DBConstants;
import teme.project.general.ReadPropertiesFile;
import teme.project.general.UiUtil;
import teme.project.item.ItemDAO;
import teme.project.item.ItemDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import static teme.project.general.DBConstants.ItemField.*;

/**
 * File upload servlet example
 */
@WebServlet("/GenerateReportServlet")
@MultipartConfig
public class GenerateReportServlet extends HttpServlet {

    /**
     * Get the list with all the item which have the status selected from the browser
     */
    private static List<ItemDTO> getItemByStatus(String itemStatus) {
        return new ItemDAO().get(ITEM_STATUS, itemStatus);
    }


    /**
     * Get the list with all the item which have the type selected from the browser
     */
    private static List<ItemDTO> getItemByType(String itemType) {
        return new ItemDAO().get(ITEM_TYPE, itemType);
    }


    /**
     * Get the list with all the item which have the type selected from the browser
     */
    private static List<ItemDTO> getItemByAuthor(String itemAuthor) {
        return new ItemDAO().get(AUTHOR, itemAuthor);
    }

    /**
     * fill the table with the item which have a selected property
     */
    private static void generatePdfInfoItem(Document document, List<ItemDTO> list) {

        try {

            PdfPTable table = new PdfPTable(5); // number of columns.

            /*
             Link: http://tutorials.jenkov.com/java-itext/table.html
             The column widths in the float array are relative widths.
             In the example above the first column is twice the width of each of the following columns.
             You can use any numbers in the width array, and they will be interpreted as relative values.
             For instance, you could have written 100, 50, 50 if you needed a finer grained control over the column sizes.
             */
            float[] columnWidths = {1f, 3f, 3f, 3f, 3f};

            setItemIdTitleAuthorTypeColumns(table, columnWidths);

            PdfPCell cell1;
            cell1 = new PdfPCell(new Paragraph("Item status"));
            table.addCell(cell1);

            for (ItemDTO itemDTO : list) {
                // set the content of the id, title, author, type columns
                setItemIdTitleAuthorTypeContentColumns(table, itemDTO);

                cell1 = new PdfPCell(new Paragraph(String.valueOf(itemDTO.getItemStatus())));
                table.addCell(cell1);
            }
            document.add(table);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * set the content of the id, title, author, type columns
     */
    private static void setItemIdTitleAuthorTypeContentColumns(PdfPTable table, ItemDTO itemDTO) {
        PdfPCell cell1;
        cell1 = new PdfPCell(new Paragraph(itemDTO.getId()));
        table.addCell(cell1);
        cell1 = new PdfPCell(new Paragraph(itemDTO.getTitle()));
        table.addCell(cell1);
        cell1 = new PdfPCell(new Paragraph(itemDTO.getAuthor()));
        table.addCell(cell1);
        cell1 = new PdfPCell(new Paragraph(String.valueOf(itemDTO.getItemType())));
        table.addCell(cell1);
    }

    /**
     * set the the id, title, author, type columns
     */
    private static void setItemIdTitleAuthorTypeColumns(PdfPTable table, float[] columnWidths) throws DocumentException {

        table.setWidths(columnWidths);

        PdfPCell cell1 = new PdfPCell(new Paragraph("Item ID"));
        table.addCell(cell1);
        cell1 = new PdfPCell(new Paragraph("Item title"));
        table.addCell(cell1);
        cell1 = new PdfPCell(new Paragraph("Item author"));
        table.addCell(cell1);
        cell1 = new PdfPCell(new Paragraph("Item type"));
        table.addCell(cell1);
    }

    /**
     * fill the table with the item which have a selected property
     */
    private static void generatePdfInfoItemOverdue(Document document) {

        try {

            List<HistoryDTO> historyDTOList = new HistoryDAO().get(DBConstants.HistoryField.OVERDUE, "1970-01-01");

            PdfPTable table = new PdfPTable(11); // number of columns.

                        /*
             Link: http://tutorials.jenkov.com/java-itext/table.html
             The column widths in the float array are relative widths.
             In the example above the first column is twice the width of each of the following columns.
             You can use any numbers in the width array, and they will be interpreted as relative values.
             For instance, you could have written 100, 50, 50 if you needed a finer grained control over the column sizes.
             */
            float[] columnWidths = {1f, 3f, 3f, 3f, 3f, 3f, 3f, 3f, 3f, 3f, 3f};

            setItemIdTitleAuthorTypeColumns(table, columnWidths);

            PdfPCell cell1 = new PdfPCell(new Paragraph("Customer name"));
            table.addCell(cell1);
            cell1 = new PdfPCell(new Paragraph("Customer email"));
            table.addCell(cell1);
            cell1 = new PdfPCell(new Paragraph("Customer phone"));
            table.addCell(cell1);
            cell1 = new PdfPCell(new Paragraph("Start date"));
            table.addCell(cell1);
            cell1 = new PdfPCell(new Paragraph("Due date"));
            table.addCell(cell1);
            cell1 = new PdfPCell(new Paragraph("Return date"));
            table.addCell(cell1);
            cell1 = new PdfPCell(new Paragraph("Overdue[days]"));
            table.addCell(cell1);

            for (HistoryDTO historyDTO : historyDTOList) {

                ItemDTO itemDTO = new ItemDAO().get(DBConstants.ItemField.ID, String.valueOf(historyDTO.getItemId())).get(0);

                CustomerDTO customerDTO = new CustomerDAO().get(DBConstants.CustomerField.ID, String.valueOf(historyDTO.getCustomerId())).get(0);

                // set the content of the id, title, author, type columns
                setItemIdTitleAuthorTypeContentColumns(table, itemDTO);
                cell1 = new PdfPCell(new Paragraph(String.valueOf(customerDTO.getName())));
                table.addCell(cell1);
                cell1 = new PdfPCell(new Paragraph(String.valueOf(customerDTO.getEmail())));
                table.addCell(cell1);
                cell1 = new PdfPCell(new Paragraph(String.valueOf(customerDTO.getPhone())));
                table.addCell(cell1);
                cell1 = new PdfPCell(new Paragraph(String.valueOf(historyDTO.getStartDate())));
                table.addCell(cell1);
                cell1 = new PdfPCell(new Paragraph(String.valueOf(historyDTO.getDueDate())));
                table.addCell(cell1);
                cell1 = new PdfPCell(new Paragraph(String.valueOf(historyDTO.getReturnDate())));
                table.addCell(cell1);
                cell1 = new PdfPCell(new Paragraph(String.valueOf(UiUtil.getLentDays(historyDTO))));
                table.addCell(cell1);
            }
            document.add(table);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }


    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        request.getRequestDispatcher("ItemHomeServlet").include(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        request.getRequestDispatcher("ItemHomeServlet").include(request, response);
    }

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws IOException if an I/O error occurs
     */
    private void processRequest(HttpServletRequest request,
                                HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        String[] reports = request.getParameterValues("report");
        if (reports != null) {
            try {
                Document document = new Document();
                String filePath = ReadPropertiesFile.readUploadedFilesPath(ReadPropertiesFile.loadConfigFromFile());
                PdfWriter.getInstance(document,
                        new FileOutputStream(filePath + File.separator + "LibraryItemsReport.pdf"));

                out.print("<p><strong><mark> !!!!!!!!! File " + filePath + File.separator + "LibraryItemsReport.pdf" + " generated successfully</mark></strong></p>");

                document.open();

                for (String report : reports) {
                    // if the option (search items by status) is checked in the browser

                    if (report.equals("itemByStatus")) {
                        Paragraph paragraph = new Paragraph();
                        String itemStatus = request.getParameter("itemstatus");
                        paragraph.add("List the items which have the status " + itemStatus);
                        document.add(paragraph);
                        generatePdfInfoItem(document, getItemByStatus(itemStatus));
                    }

                    // if the option (search the items which are overdue) is checked in the browser
                    else if (report.equals("itemOverdue")) {
                        Paragraph paragraph = new Paragraph();
                        paragraph.add("List the items which are overdue");
                        document.add(paragraph);
                        generatePdfInfoItemOverdue(document);
                    }

                    // if the option (search the items by type) is checked in the browser
                    else if (report.equals("itemByType")) {
                        Paragraph paragraph = new Paragraph();
                        String itemType = request.getParameter("itemtype");
                        paragraph.add("List the items which have the type " + itemType);
                        document.add(paragraph);
                        generatePdfInfoItem(document, getItemByType(itemType));
                    }
                    // if the option (Select the items by author)
                    else if (report.equals("itemByAuthor")) {
                        Paragraph paragraph = new Paragraph();
                        String itemAuthor = request.getParameter("itemauthor");
                        paragraph.add("List the items which have the author " + itemAuthor);
                        document.add(paragraph);
                        generatePdfInfoItem(document, getItemByAuthor(itemAuthor));
                    }
                }
                document.close();

            } catch (DocumentException e) {
                e.printStackTrace();
            }
        }
    }
}