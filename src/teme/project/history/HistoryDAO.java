package teme.project.history;

import teme.project.general.AbstractDao;
import teme.project.general.DBConstants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class HistoryDAO extends AbstractDao<HistoryDTO, DBConstants.HistoryField> {

    //DB table related constants
    private static final String TABLE_NAME = "history";
    private static final String ITEM_ID = "itemid";
    private static final String CUSTOMER_ID = "customerid";
    private static final String START_DATE = "startdate";
    private static final String DUE_DATE = "duedate";
    private static final String RETURN_DATE = "returndate";

    @Override
    protected List<HistoryDTO> executeSelect(Connection c) {
        String sql = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + ITEM_ID;
        List<HistoryDTO> results = new ArrayList<>();
        try (PreparedStatement ps = c.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                extractHistoryFromResult(results, rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }


    @Override
    protected String authenticate(Connection c, String username, String password) {
        return null;
    }

    @Override
    protected List<HistoryDTO> executeSelect(Connection c, DBConstants.HistoryField option, String historyValue) {
        String sql = "";
        List<HistoryDTO> results = new ArrayList<>();
        switch (option) {
            case ITEM_ID:
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + ITEM_ID + " = ?";
                break;
            case CUSTOMER_ID:
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + CUSTOMER_ID + " = ?";
                break;
            case START_DATE:
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + START_DATE + " = ?";
                break;
            case DUE_DATE:
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + DUE_DATE + " = ?";
                break;
            case RETURN_DATE:
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + RETURN_DATE + " = ?";
                break;
            case OVERDUE:
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + RETURN_DATE + " != ? AND " + RETURN_DATE + " > " + DUE_DATE;
                break;
            default:
                System.out.println("HistoryDAO - no option");
                break;
        }
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setString(1, historyValue);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                extractHistoryFromResult(results, rs);
            }
            return results;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void extractHistoryFromResult(List<HistoryDTO> results, ResultSet rs) throws SQLException {
        results.add(new HistoryDTO(rs.getInt(ITEM_ID), rs.getInt(CUSTOMER_ID),
                rs.getDate(START_DATE), rs.getDate(DUE_DATE), rs.getDate(RETURN_DATE)));
    }

    @Override
    protected boolean executeSelect(Connection c, HistoryDTO userObject) {
        return false;
    }

    @Override
    protected void executeInsert(Connection c, HistoryDTO object) {
        String sql = "INSERT INTO " + TABLE_NAME +
                " (" + ITEM_ID + ", " + CUSTOMER_ID + ", " + START_DATE + ", " + DUE_DATE + ", " + RETURN_DATE + ") VALUES(?,?,?,?,?)";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setInt(1, object.getItemId());
            ps.setInt(2, object.getCustomerId());
            ps.setDate(3, object.getStartDate());
            ps.setDate(4, object.getDueDate());
            ps.setDate(5, object.getReturnDate());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void executeUpdate(Connection c, HistoryDTO object) {
        String sql = "UPDATE " + TABLE_NAME + " SET " + RETURN_DATE + " = ? WHERE " +
                ITEM_ID + " = ? AND " + CUSTOMER_ID + " = ? AND " + START_DATE + " = ? AND " + DUE_DATE + " = ?";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setDate(1, object.getReturnDate());
            ps.setInt(2, object.getItemId());
            ps.setInt(3, object.getCustomerId());
            ps.setDate(4, object.getStartDate());
            ps.setDate(5, object.getDueDate());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void executeDelete(Connection c, HistoryDTO object) {
        String sql = "DELETE FROM " + TABLE_NAME + " WHERE id = ?";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setInt(1, object.getItemId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
