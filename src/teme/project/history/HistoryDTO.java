package teme.project.history;

import java.sql.Date;
import java.util.Objects;

public class HistoryDTO {
    private int itemId;
    private int customerId;
    private Date startDate;         // the date when the item was lent
    private Date dueDate;           // the dte when the item shall be returned
    private Date returnDate;       // the date when the item was returned


    public HistoryDTO(int itemId, int customerId, Date startDate, Date dueDate, Date returnDate) {
        this.itemId = itemId;
        this.customerId = customerId;
        this.startDate = startDate;
        this.dueDate = dueDate;
        this.returnDate = returnDate;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HistoryDTO that = (HistoryDTO) o;
        return itemId == that.itemId &&
                customerId == that.customerId &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(dueDate, that.dueDate) &&
                Objects.equals(returnDate, that.returnDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemId, customerId, startDate, dueDate, returnDate);
    }

    @Override
    public String toString() {
        return "History{" +
                "itemId=" + itemId +
                ", customerId=" + customerId +
                ", startDate=" + startDate +
                ", dueDate=" + dueDate +
                ", returnDate=" + returnDate +
                '}';
    }
}
