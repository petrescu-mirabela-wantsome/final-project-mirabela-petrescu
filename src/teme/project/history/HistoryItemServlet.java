package teme.project.history;

import teme.project.customer.CustomerDAO;
import teme.project.customer.CustomerDTO;
import teme.project.general.DBConstants;
import teme.project.general.UiUtil;
import teme.project.item.ItemDAO;
import teme.project.item.ItemDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static teme.project.general.DBConstants.HistoryField.ITEM_ID;
import static teme.project.general.DBConstants.UserType.LIBRARIAN;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/HistoryItemServlet")
public class HistoryItemServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */


    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser the type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, LIBRARIAN)) {

            /*
             *  PAGE INFORMATION
             *  LOGOUT BUTTON
             *  BACK BUTTON
             */
            out.println("<!DOCTYPE html><html>" +
                    "<head><title>Library Item History</title></head>" +
                    UiUtil.servletSessionInfo(session) +
                    "<body><h1>Library Item History View</h1>" +
                    "<a href='LogoutUserServlet' class='btn btn-primary' role='button'>Logout</a><br><br>" +
                    "<a href='ItemComparatorTableServlet' class='btn btn-primary' role='button'>Back (Library Items table view)</a><br><br>");

            /*
             * HISTORY TABLE CONTENT
             * get the id value (from the browser) to find the history from the database
             */
            getHistoryTableView(out, request.getParameter("id"));

        } else {
            out.print("!!!! Sorry. Administrator already logged in. Please try again");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }

    /**
     * HISTORY TABLE CONTENT
     */
    private void getHistoryTableView(PrintWriter out, String id) {

        List<HistoryDTO> historyDTOList = new HistoryDAO().get(ITEM_ID, id);

        CustomerDAO customerDAO = new CustomerDAO();
        ItemDAO itemDAO = new ItemDAO();

        out.println("<br><br><table class='table table-bordered table-striped'><tr>" +
                "   <th><ins>ItemId</th>" +
                "   <th><ins>ItemTitle</th>" +
                "   <th><ins>ItemAuthor</th>" +
                "   <th><ins>ItemType</th>" +
                "   <th><ins>CustomerName</th>" +
                "   <th><ins>CustomerEmail</th>" +
                "   <th><ins>StartDate</th>" +
                "   <th><ins>DueDate</th>" +
                "   <th><ins>ReturnDate</th>" +
                "   <th><ins>Overdue[days]</th></tr>");

        for (HistoryDTO historyDTO : historyDTOList) {

            ItemDTO itemDTO = itemDAO.get(DBConstants.ItemField.ID, String.valueOf(historyDTO.getItemId())).get(0);

            CustomerDTO customerDTO = customerDAO.get(DBConstants.CustomerField.ID, String.valueOf(historyDTO.getCustomerId())).get(0);

            out.println("<tr><td align=\"center\">" + id +
                    "   </td><td align=\"center\">" + itemDTO.getTitle() +
                    "   </td><td align=\"center\">" + itemDTO.getAuthor() +
                    "   </td><td align=\"center\">" + itemDTO.getItemType() +
                    "   </td><td align=\"center\">" + customerDTO.getName() +
                    "   </td><td align=\"center\">" + customerDTO.getEmail() +
                    "   </td><td align=\"center\">" + historyDTO.getStartDate() +
                    "   </td><td align=\"center\">" + historyDTO.getDueDate());

            /*
             * FIND THE ITEM RETURN DATE (ITEM HISTORY CASE)
             * - if the item is still lent, then the return date value appears as STILL LENT
             */
            UiUtil.getHistoryReturnDate(out, historyDTO, itemDTO);
        }
        out.print("</table></body>");
    }

}
