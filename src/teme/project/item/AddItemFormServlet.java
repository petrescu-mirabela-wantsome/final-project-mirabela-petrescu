package teme.project.item;

import teme.project.general.DBConstants;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static teme.project.general.DBConstants.UserType.LIBRARIAN;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/AddItemFormServlet")
public class AddItemFormServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */


    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser the type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, LIBRARIAN)) {

            out.print("<!DOCTYPE html><html>" +
                    "<head><title>Edit Item Library Form</title></head>" +
                    UiUtil.servletSessionInfo(session) +
                    "<body><br><br><a href='LogoutUserServlet' class='btn btn-primary' role='button'>Logout</a>" +
                    "<br><br><a href='ItemHomeServlet' class='btn btn-primary' role='button'>Back (Library Item view)</a></ul><br><br>" +

                    "<form action='AddItemServlet' method='post' style='width:300px'>" +
                    "<table class='table table-bordered table-striped'>" +

                    "<tr><td><label for=\"title1\">Title</label></td>" +
                    "<td><input type=\"text\" class=\"form-control\" name=\"title\" id=\"title1\" placeholder=\"Title\"/>" +
                    "</td></tr>" +

                    "<tr><td><label for=\"author1\">Author</label></td>" +
                    "<td><input type=\"text\" class=\"form-control\" name=\"author\" id=\"author1\" placeholder=\"Author\"/>" +
                    "</td></tr>" +

                    "<tr><td><label for='itemtype1'>Item type</label>" +
                    "<td><select name=\"itemtype\">");

            /*
             * CREATE THE DROP DOWN LIST FOR THE ITEM TYPE ENUM
             */
            UiUtil.createEnumDropDownList(out, DBConstants.ItemType.values(), DBConstants.ItemType.values()[0]);

            out.print("<tr><td><label for='itemstatus1'>Item status</label>" +
                    "<td><select name=\"itemstatus\">");

            /*
             *  CREATE THE DROP DOWN LIST FOR THE ITM STATUS ENUM
             */
            UiUtil.createEnumDropDownList(out, DBConstants.ItemStatus.values(), DBConstants.ItemStatus.values()[0]);

            out.print("</table><br><button type='submit' class='btn btn-primary'>Add</button>" +
                    "<button type='reset' class='btn btn-primary'>Reset</button>" +
                    "</form></body>");
        } else {
            out.print("!!!! Sorry. Administrator already logged in. Please try again");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}
