package teme.project.item;

import teme.project.general.DBConstants;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static teme.project.general.DBConstants.UserType.LIBRARIAN;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/AddItemServlet")
public class AddItemServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser hte type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        /*
         * // http://tutorials.jenkov.com/java-servlets/httpsession.html
         * The HttpSession object represents a user session.
         * A user session contains information about the user across multiple HTTP requests.
         */
        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, LIBRARIAN)) {

            // get the new admin info written by the user in the browser
            String title = request.getParameter("title");
            String author = request.getParameter("author");

            // Check if the information is introduced in the browser
            if (UiUtil.isEmptyString(title) &&
                    UiUtil.isEmptyString(author)) {

                DBConstants.ItemType itemType = DBConstants.ItemType.valueOf(request.getParameter("itemtype"));
                DBConstants.ItemStatus itemStatus = DBConstants.ItemStatus.valueOf(request.getParameter("itemstatus"));

                /*
                The RequestDispatcher class enables your servlet to "call" another servlet from inside another servlet.
                The other servlet is called as if an HTTP request was sent to it by a browser.
                Link: http://tutorials.jenkov.com/java-servlets/requestdispatcher.html
                */
                new ItemDAO().insert(new ItemDTO(title, author, itemType, itemStatus));      // write into user table of the data base the info written by the user in the browser

            /*
            The RequestDispatcher class enables your servlet to "call" another servlet from inside another servlet.
            The other servlet is called as if an HTTP request was sent to it by a browser.
            Link: http://tutorials.jenkov.com/java-servlets/requestdispatcher.html
            */

                // the information is correct and introduced into the database ==> current servlet is calling the AdminHome servlet
                request.getRequestDispatcher("ItemHomeServlet").forward(request, response);
            } else {
                // the current servlet is calling AddItemFormServlet (because the information is not filled in correctly) ==> NOT introduced into the data base
                // the user has to introduce again the information into the browser
                request.getRequestDispatcher("AddItemFormServlet").forward(request, response);
            }
        } else {
            out.print("!!!! Sorry. Administrator already logged in. Please try again");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}
