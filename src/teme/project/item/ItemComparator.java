package teme.project.item;


import java.util.Comparator;

/*
LINK: https://howtodoinjava.com/sort/groupby-sort-multiple-comparators/
 */

class ItemComparator {

    static class UserComparatorByTitle implements Comparator<ItemDTO> {

        @Override
        public int compare(ItemDTO o1, ItemDTO o2) {
            return o1.getTitle().toLowerCase().compareTo(o2.getTitle().toLowerCase());
        }
    }


    static class UserComparatorByAuthor implements Comparator<ItemDTO> {

        @Override
        public int compare(ItemDTO o1, ItemDTO o2) {
            return o1.getAuthor().toLowerCase().compareTo(o2.getAuthor().toLowerCase());
        }
    }


    static class UserComparatorByItemType implements Comparator<ItemDTO> {

        @Override
        public int compare(ItemDTO o1, ItemDTO o2) {
            return String.valueOf(o1.getItemType()).toLowerCase().compareTo(String.valueOf(o2.getItemType()).toLowerCase());
        }
    }

    static class UserComparatorByItemStatus implements Comparator<ItemDTO> {

        @Override
        public int compare(ItemDTO o1, ItemDTO o2) {

            return String.valueOf(o1.getItemStatus()).toLowerCase().compareTo(String.valueOf(o2.getItemStatus()).toLowerCase());
        }
    }

}
