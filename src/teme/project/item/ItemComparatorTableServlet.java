package teme.project.item;

import teme.project.general.DBConstants;
import teme.project.general.GroupBySorted;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static teme.project.general.DBConstants.UserType.LIBRARIAN;

/*
 * IT SHALL LIST THE USER TABLE, SORTED BY THE OPTIONS SELECTED FROM THE BROWSER
 */

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/ItemComparatorTableServlet")
public class ItemComparatorTableServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser hte type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        /*
         * Link: http://tutorials.jenkov.com/java-servlets/httpsession.html
         * The HttpSession object represents a user session.
         * A user session contains information about the user across multiple HTTP requests.
         */
        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, LIBRARIAN)) {

            // Show in the browser the current user and user type
            // show in the browser the logout button
            // show in the browser the back button
            out.print(UiUtil.servletSessionInfo(session) +
                    "<h1>Library Items Table</h1>" +
                    "<a href='LogoutUserServlet' class='btn btn-primary' role='button'>Logout</a><br><br>" +
                    "<a href='ItemHomeServlet' class='btn btn-primary' role='button'>Back (Item Library Home)</a><br><br>");


            /*
             * SORT THE LIBRARY ITEMS = getLibraryItemsSort(request, out)
             * LIBRARY VIEW TABLE
             */
            UiUtil.itemViewTable(out, getLibraryItemsSort(request, out));

        } else {
            out.print("<p><strong><mark> !!!!!!!!! Sorry. User already logged in. Please try again.</mark></strong></p>");
            request.getRequestDispatcher("index.html").include(request, response);
        }

    }


    /**
     * SORT THE LIBRARY ITEMS
     */
    private List<ItemDTO> getLibraryItemsSort(HttpServletRequest request, PrintWriter out) {
        // get the username selected by the user in the browser
        String[] itemComparator = request.getParameterValues("comparator");

        List<ItemDTO> itemDTOList = new ItemDAO().get();
        List<Comparator<ItemDTO>> listComparators = new ArrayList<>();

        // If The sort button is pressed AND
        // there is at least one sort option selected in the browser
        if (itemComparator != null) {
            for (String report : itemComparator) {
                switch (report) {
                    case "itemCompByTitle":
                        listComparators.add(new ItemComparator.UserComparatorByTitle());
                        out.print("<p><strong><mark> !!!!!!!!! Items sorted by title.</mark></strong></p>");
                        break;
                    case "itemCompByAuthor":
                        listComparators.add(new ItemComparator.UserComparatorByAuthor());
                        out.print("<p><strong><mark> !!!!!!!!! Items sorted by author.</mark></strong></p>");
                        break;
                    case "itemCompByStatus":
                        listComparators.add(new ItemComparator.UserComparatorByItemStatus());
                        out.print("<p><strong><mark> !!!!!!!!! Items sorted by status.</mark></strong></p>");
                        break;
                    case "itemCompByType":
                        listComparators.add(new ItemComparator.UserComparatorByItemType());
                        out.print("<p><strong><mark> !!!!!!!!! Items sorted by type.</mark></strong></p>");
                        break;
                }
            }
            // If there is no sort options selected in the browser AND the sort button is pressed
        } else {
            // default comparator by id
            out.print("<p><strong><mark> !!!!!!!!! Items sorted by ID.</mark></strong></p>");
        }

        // Sort the items applying the group of comparators
        itemDTOList.sort(new GroupBySorted<>(listComparators));

        return itemDTOList;
    }
}
