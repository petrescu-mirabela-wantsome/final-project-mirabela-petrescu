package teme.project.item;

import teme.project.general.AbstractDao;
import teme.project.general.DBConstants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ItemDAO extends AbstractDao<ItemDTO, DBConstants.ItemField> {

    //DB table related constants
    private static final String TABLE_NAME = "item";
    private static final String ID = "id";
    private static final String TITLE = "title";
    private static final String AUTHOR = "author";
    private static final String ITEM_TYPE = "itemtype";
    private static final String ITEM_STATUS = "itemstatus";

    @Override
    protected List<ItemDTO> executeSelect(Connection c) {
        String sql = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + ID;
        List<ItemDTO> results = new ArrayList<>();
        try (PreparedStatement ps = c.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                extractItemFromResult(results, rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    private void extractItemFromResult(List<ItemDTO> results, ResultSet rs) throws SQLException {
        results.add(new ItemDTO(rs.getInt(ID), rs.getString(TITLE), rs.getString(AUTHOR),
                DBConstants.ItemType.valueOf(rs.getString(ITEM_TYPE)), DBConstants.ItemStatus.valueOf(rs.getString(ITEM_STATUS))));
    }


    @Override
    protected String authenticate(Connection c, String username, String password) {
        return null;
    }


    @Override
    protected List<ItemDTO> executeSelect(Connection c, DBConstants.ItemField option, String itemOption) {
        /*
         * ITEM table options (select by):
         * -1 = no option
         * 1 = item id
         * 2 = item title
         * 3 = item author
         * 4 = item type
         * 5 = item status
         */
        String sql = "";
        List<ItemDTO> results = new ArrayList<>();
        switch (option) {
            case ID:
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = ?";
                break;
            case TITLE:
                // LINK: https://alvinalexander.com/blog/post/jdbc/jdbc-preparedstatement-select-like
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE UPPER(" + TITLE + ") LIKE ?";
                // going to do a search using "upper" and add the specific criteria
                itemOption = "%" + itemOption.toUpperCase() + "%";
                break;
            case AUTHOR:
                // LINK: https://alvinalexander.com/blog/post/jdbc/jdbc-preparedstatement-select-like
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE UPPER(" + AUTHOR + ") LIKE ?";
                // going to do a search using "upper" and add the specific criteria
                itemOption = "%" + itemOption.toUpperCase() + "%";
                break;
            case ITEM_TYPE:
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + ITEM_TYPE + " = ?";
                break;
            case ITEM_STATUS:
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + ITEM_STATUS + " = ?";
                break;
            default:
                System.out.println("Item - no option");
                break;
        }
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setString(1, itemOption);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                extractItemFromResult(results, rs);
            }
            return results;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected boolean executeSelect(Connection c, ItemDTO userObject) {
        return false;
    }

    @Override
    protected void executeInsert(Connection c, ItemDTO object) {
        String sql = "INSERT INTO " + TABLE_NAME +
                " (" + TITLE + ", " + AUTHOR + ", " + ITEM_TYPE + ", " + ITEM_STATUS + ") VALUES(?,?,?,?)";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setString(1, object.getTitle());
            ps.setString(2, object.getAuthor());
            ps.setString(3, object.getItemType().name());
            ps.setString(4, object.getItemStatus().name());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void executeUpdate(Connection c, ItemDTO object) {
        String sql = "UPDATE " + TABLE_NAME +
                " SET " + TITLE + " = ?, " + AUTHOR + " = ?, " + ITEM_TYPE + " = ?, " + ITEM_STATUS + " = ? WHERE " + ID + " = ?";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setString(1, object.getTitle());
            ps.setString(2, object.getAuthor());
            ps.setString(3, object.getItemType().name());
            ps.setString(4, object.getItemStatus().name());
            ps.setInt(5, object.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void executeDelete(Connection c, ItemDTO object) {
        String sql = "DELETE FROM " + TABLE_NAME + " WHERE " + ID + " = ?";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setInt(1, object.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
