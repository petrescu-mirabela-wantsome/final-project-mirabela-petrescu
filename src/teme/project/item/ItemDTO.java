package teme.project.item;

import teme.project.general.DBConstants;

import java.util.Objects;

public class ItemDTO {

    private int id;
    private String title;
    private String author;
    private DBConstants.ItemType itemType;
    private DBConstants.ItemStatus itemStatus;

    public ItemDTO() {
    }

    public ItemDTO(String title, String author, DBConstants.ItemType itemType, DBConstants.ItemStatus itemStatus) {
        this.title = title;
        this.author = author;
        this.itemType = itemType;
        this.itemStatus = itemStatus;
    }

    public ItemDTO(int id, String title, String author, DBConstants.ItemType itemType, DBConstants.ItemStatus itemStatus) {
        this(title, author, itemType, itemStatus);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public DBConstants.ItemType getItemType() {
        return itemType;
    }

    public void setItemType(DBConstants.ItemType itemType) {
        this.itemType = itemType;
    }

    public DBConstants.ItemStatus getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(DBConstants.ItemStatus itemStatus) {
        this.itemStatus = itemStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemDTO itemDTO = (ItemDTO) o;
        return id == itemDTO.id &&
                Objects.equals(title, itemDTO.title) &&
                Objects.equals(author, itemDTO.author) &&
                itemType == itemDTO.itemType &&
                itemStatus == itemDTO.itemStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, author, itemType, itemStatus);
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", itemType=" + itemType +
                ", itemStatus=" + itemStatus +
                '}';
    }
}
