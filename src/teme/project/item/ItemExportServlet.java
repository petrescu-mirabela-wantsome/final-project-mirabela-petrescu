/*
 * Copyright (c) 2014 Oracle and/or its affiliates. All rights reserved.
 * <p>
 * You may not modify, use, reproduce, or distribute this software except in
 * compliance with  the terms of the License at:
 * https://github.com/javaee/tutorial-examples/LICENSE.txt
 */

package teme.project.item;

/*
 * LINK: https://docs.oracle.com/javaee/6/tutorial/doc/glraq.html
 * LINK: https://github.com/javaee/tutorial-examples/blob/master/web/servlet/fileupload/src/main/java/javaeetutorial/fileupload/FileUploadServlet.java
 */

import teme.project.general.DBConstants;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.function.Function;

import static teme.project.general.DBConstants.UserType.LIBRARIAN;

/**
 * File upload servlet example
 */
@WebServlet("/ItemExportServlet")
@MultipartConfig
public class ItemExportServlet extends HttpServlet {


    /**
     * CREATE A STRING FROM AN OBJECT ADDING , BETWEEN OBJECT FIELDS
     */
    private static Function<ItemDTO, String> mapToString =
            obj -> new StringBuilder()
                    .append(obj.getId()).append(",")
                    .append(obj.getTitle()).append(",")
                    .append(obj.getAuthor()).append(",")
                    .append(obj.getItemStatus()).append(",")
                    .append(obj.getItemType()).toString();

    /**
     * READ THE DATA FROM THE CSV FILE, IMPORTED IN THE BROWSER
     * WRITE THE DATA INTO AN ARRAY LIST
     */
    private static void writeItemFromDatabase(OutputStream out) throws IOException {

        /*
         * ALL THE ITEMS FROM THE DATABASE ARE CONVERTED TO A STRING:
         * list of items from database = new ItemDAO().get();
         * - each item field separated by "," => mapToString.apply(i)
         * - each item separated by \n => Collectors.joining("\n")
         *
         * list of items from database = new ItemDAO().get()
         */

        String stringItemFromDatabase = UiUtil.getStringFromObject(new ItemDAO().get(), mapToString);


        /*
         *  WRITE THE ITEMS FROM THE DATABASE (String value) INTO A .CSV FILE
         */
        UiUtil.writeToFileInfo(stringItemFromDatabase, out);
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        request.getRequestDispatcher("ItemHomeServlet").include(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        request.getRequestDispatcher("ItemHomeServlet").include(request, response);
    }

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws IOException if an I/O error occurs
     */
    private void processRequest(HttpServletRequest request,
                                HttpServletResponse response)
            throws IOException, ServletException {
        response.setContentType("text/html;charset=UTF-8");


        // Create path components to save the file
        final String path = request.getParameter("exportdestination");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter writer = response.getWriter();

        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, LIBRARIAN)) {

            try {
            /*
            EXPORT THE INFORMATION FROM THE DATABASE INTO A CSV FILE
             */
                writeItemFromDatabase(new FileOutputStream(new File(path + File.separator + "itemExport.csv")));

                writer.println("<strong><mark>!!!!!!! New file " + "itemExport.csv" + " created at " + path +
                        "<br>!!!!!!! It contains the item table information from database</strong></mark><br>");


            } catch (FileNotFoundException fne) {
                writer.println("<strong><mark>!!!!!!! You either did not specify a file to upload or are "
                        + "trying to upload a file to a protected or nonexistent "
                        + "location.</strong></mark>");
                writer.println("<br/> <strong><mark>!!!!!!!  ERROR: " + fne.getMessage() + "</strong></mark>");
            }
        } else {
            writer.print("!!!! Sorry. Administrator already logged in. Please try again");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }

}