package teme.project.item;

import teme.project.customer.CustomerDAO;
import teme.project.customer.CustomerDTO;
import teme.project.general.DBConstants;
import teme.project.general.ReadPropertiesFile;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static teme.project.general.DBConstants.ItemField.ITEM_STATUS;
import static teme.project.general.DBConstants.ItemStatus.AVAILABLE;
import static teme.project.general.DBConstants.ItemStatus.LENT;
import static teme.project.general.DBConstants.UserType.LIBRARIAN;

/*
This Servlet shall update the database with the librarian data written by the user in the browser
(administrator has rights to introduce a librarian)
 */

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */

@WebServlet("/ItemHomeServlet")
public class ItemHomeServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * LENT A LIBRARY ITEM
     */
    private static void libraryItemLent(PrintWriter out, List<ItemDTO> itemDTOList, List<CustomerDTO> customerDTOList) {
        // Link: https://www.quora.com/How-do-I-get-a-value-from-a-drop-down-list-in-JSP
        // Add the drop down list for the items id

        out.println("<br><strong><mark>Lent a Library Item </strong></mark><form action=\"LentItemServlet\" method=\"POST\">");

        out.print("<table class='table table-bordered table-striped'>" +
                "<tr><th><ins>Item ID</th><th><ins>Customer ID</th><th><ins>Lent date</th><th><ins>Due Date</th></tr>");

        if (itemDTOList.size() > 0) {
            /* name	Defines a name for the drop-down list
            Link: https://www.w3schools.com/tags/tag_select.asp */
            out.println("<tr><td align=\"center\"><select name=\"itemIdLent\">");
            for (ItemDTO itemDTO : itemDTOList) {
                // Display only the AVAILABLE items
                if (itemDTO.getItemStatus().equals(AVAILABLE)) {
                    // Link: https://www.w3schools.com/tags/att_option_selected.asp - the value from database will be selected from the list
                    out.println("<option value = " + itemDTO.getId() + ">" +
                            itemDTO.getAuthor() + " - " + itemDTO.getTitle() + " - " + itemDTO.getItemType() + "</option>");
                }
            }
            out.print("</select></td>");

            // Add the drop down list for the customers id
            if (customerDTOList.size() > 0) {
            /* name	Defines a name for the drop-down list
            Link: https://www.w3schools.com/tags/tag_select.asp */
                out.println("<td align=\"center\"><select required name=\"customerIdLent\">");
                for (CustomerDTO customerDTO : customerDTOList) {
                    // Link: https://www.w3schools.com/tags/att_option_selected.asp - the value from database will be selected from the list
                    out.println("<option value = " + customerDTO.getId() + ">" +
                            customerDTO.getName() + " - " + customerDTO.getEmail() + "</option>");
                }
                out.print("</select></td>");
            }
            // if there is no customer registered
            else {
                //out.print("<td align=\"center\">Please register the customer</td>");
                out.print("<td align=\"center\"><input type=\"text\" value=\"Please register the customer\" readonly/></td>");
            }

            /*
             * DATE PICKER
             * LINK: https://www.javaworld.com/article/2073678/html5-date-picker.html
             */
            out.print("<td align=\"center\"><input type=\"date\" name=\"startDateLent\"/></td>");
            out.print("<td align=\"center\"><input type=\"date\" name=\"dueDateLent\"/></td></tr>");
        }
        out.print("</table><input type=\"submit\"/></form>");
    }

    /**
     * ADD ITEM FROM CSV
     */
    private static void libraryItemAdd(PrintWriter out, String path) {
        /*
         * LINK: http://www.tutorialspoint.com/servlets/servlets-file-uploading.htm
         * https://www.baeldung.com/upload-file-servlet
         * https://www.w3schools.com/html/html_formatting.asp
         */
        out.println("<br><strong><mark>Add items from .csv file</strong></mark>" +
                "        <form method=\"POST\" action=\"FileUploadServlet\" enctype=\"multipart/form-data\" >" +
                "            File:" +
                "            <input type=\"file\" name=\"file\" id=\"file\" /> <br/>" +
                "            Destination:" +
                "            <input type=\"text\" value=\"" + path + "\" name=\"destination\"/>" +
                "            <input type=\"submit\" value=\"Upload\" name=\"upload\" id=\"upload\" />" +
                "        </form>");
    }

    /**
     * RETURN A LIBRARY ITEM
     */
    private static void libraryItemReturn(PrintWriter out) {
        // Link: https://www.quora.com/How-do-I-get-a-value-from-a-drop-down-list-in-JSP
        // Add the drop down list for the items id
        out.println("<br><strong><mark>Return a Library Item </strong></mark><form action=\"ReturnItemServlet\" method=\"POST\">");
        out.print("<table class='table table-bordered table-striped'>" +
                "<tr><th><ins>Item ID</th><th><ins>Return Date</th></tr>");

        /*
        List only the items which are have status LENT
         */
        List<ItemDTO> itemDTOList = new ItemDAO().get(ITEM_STATUS, String.valueOf(LENT));
        if (itemDTOList.size() > 0) {
            /* name	Defines a name for the drop-down list
            Link: https://www.w3schools.com/tags/tag_select.asp */
            out.println("<tr><td align=\"center\"><select required name=\"itemIdReturn\">");
            for (ItemDTO itemDTO : itemDTOList) {
                // Display only the lent items
                // Link: https://www.w3schools.com/tags/att_option_selected.asp - the value from database will be selected from the list
                out.println("<option value = " + itemDTO.getId() + ">" +
                        itemDTO.getTitle() + " - " + itemDTO.getAuthor() + " - " + itemDTO.getItemType() + "</option>");
            }
            out.print("</select></td>");

            /*
             * DATE PICKER
             * LINK: https://www.javaworld.com/article/2073678/html5-date-picker.html
             */
            out.print("<td align=\"center\"><input type=\"date\" name=\"returnDateReturn\"/></td></tr>");
        }
        out.print("</table><input type=\"submit\"/></form>");
    }

    /**
     * EXPORT USER INFORMATION FROM THE DATABASE INTO A CSV FILE
     * LINK: http://www.tutorialspoint.com/servlets/servlets-file-uploading.htm
     * https://www.baeldung.com/upload-file-servlet
     */
    private static void exportItemFromDatabase(PrintWriter out, String path) {
        out.print("<br><strong><mark>Export Library Items from Database (to csv file)</strong></mark><br>" +
                "        <form method=\"POST\" action=\"ItemExportServlet\" enctype=\"multipart/form-data\" >" +
                "            File exported in path:" +
                "            <input type=\"text\" value=\"" + path + "\" name=\"exportdestination\"/>" +
                "            <input type=\"submit\" value=\"Export\" name=\"upload\" id=\"upload\" />" +
                "        </form>");
    }

    /**
     * USER TABLE - SORTED BY THE OPTIONS SELECTED IN THE BROWSER
     */
    private static void itemViewComparator(PrintWriter out) {
        out.print("<br><strong><mark>View Library Items (database information)</strong></mark>");

        // LINK: https://www.codejava.net/java-ee/servlet/handling-html-form-data-with-java-servlet
        // LINK: https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_select_multiple

        // comparator - will contain all the selected checkboxes from the browser => it is an array
        out.print(" <form action='ItemComparatorTableServlet' method='post' style='width:300px'>" +
                "   Sort the items<br>" +
                "   <input type=\"checkbox\" name=\"comparator\" value=\"itemCompByTitle\" > Sort the items by title<br>" +
                "   <input type=\"checkbox\" name=\"comparator\" value=\"itemCompByAuthor\" > Sort the items by author<br>" +
                "   <input type=\"checkbox\" name=\"comparator\" value=\"itemCompByType\" > Sort the items by type<br>" +
                "   <input type=\"checkbox\" name=\"comparator\" value=\"itemCompByStatus\" > Sort the items by status<br>" +
                "   <button type=\"submit\" class=\"btn btn-primary\">View Library Items</button>" +
                "   <button type='reset' class='btn btn-primary'>Reset the sort</button></form></body>");
    }

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser hte type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        /*
         * // http://tutorials.jenkov.com/java-servlets/httpsession.html
         * The HttpSession object represents a user session.
         * A user session contains information about the user across multiple HTTP requests.
         */
        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, LIBRARIAN)) {


            /*
             * LOGOUT
             */
            out.print(UiUtil.servletSessionInfo(session) +
                    "<h2>Librarian View - Library Item View</h2>" +
                    "<a href='LogoutUserServlet' class='btn btn-primary' role='button'>Logout</a><br><br>");


            /*
             * GO BACK
             */
            out.print("<a href='LibrarianHomeServlet' class='btn btn-primary' role='button'>Back (Librarian Home view)</a><br><br>");


            /*
             * GENERATE REPORT
             */
            out.print("<a href='GenerateReportFormServlet' class='btn btn-primary' role='button'>Generate Report</a><br>");

            /*
             * ADD LIBRARY ITEM
             */
            out.print("<br><a href='AddItemFormServlet' class='btn btn-primary' role='button'>Add New Library Item</a><br>");

            /*
             * ADD ITEM FROM CSV
             */
            libraryItemAdd(out, ReadPropertiesFile.readUploadedFilesPath(ReadPropertiesFile.loadConfigFromFile()));


            /*
             * EXPORT USER INFORMATION FROM THE DATABASE INTO A CSV FILE
             * Path of the export file = ReadPropertiesFile.readUploadedFilesPath(ReadPropertiesFile.loadConfigFromFile())
             * */
            exportItemFromDatabase(out, ReadPropertiesFile.readUploadedFilesPath(ReadPropertiesFile.loadConfigFromFile()));


            /*
             * LENT A LIBRARY ITEM
             */
            libraryItemLent(out, new ItemDAO().get(), new CustomerDAO().get());


            /*
             * RETURN A LIBRARY ITEM
             */
            libraryItemReturn(out);

            /*
             * VIEW THE LIBRARY ITEMS INTO A TABLE
             */
            itemViewComparator(out);

        } else {
            out.print("!!!! Sorry. Administrator already logged in. Please try again");
            request.getRequestDispatcher("index.html").include(request, response);
        }

    }
}
