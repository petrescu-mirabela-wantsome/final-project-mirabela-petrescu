package teme.project.item;

import teme.project.general.DBConstants;
import teme.project.general.UiUtil;
import teme.project.history.HistoryDAO;
import teme.project.history.HistoryDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.stream.Collectors;

import static teme.project.general.DBConstants.HistoryField.CUSTOMER_ID;
import static teme.project.general.DBConstants.ItemField.ID;
import static teme.project.general.DBConstants.ItemStatus.AVAILABLE;
import static teme.project.general.DBConstants.ItemStatus.LENT;
import static teme.project.general.DBConstants.MAX_BOOKS_ISSUED_TO_A_USER;
import static teme.project.general.DBConstants.UserType.LIBRARIAN;
import static teme.project.general.DateFormatter.getSqlDate;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/LentItemServlet")
public class LentItemServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /*
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */

    /**
     * FIND THE NUMBER OF LENT ITEMS OF A SPECIFIC CUSTOMER
     * There should be a maximum limit MAX_BOOKS_ISSUED_TO_A_USER (5) on how many books a member can lend.
     * - get all the items lent by the current customer
     * - for the customer if of history get the item id from history
     * - each item id obtained from history will be checked to see if its item status is lent
     * - count all these obtained items (which are lent by the current user)
     */

    private static long getOneCustomerLentItems(HistoryDAO historyDAO, ItemDAO itemDAO, String customerInfo) {
        return historyDAO.get(CUSTOMER_ID, customerInfo)
                .stream()
                .map(s -> s.getItemId())
                .collect(Collectors.toList())
                .stream()
                .filter(s -> itemDAO.get(ID, String.valueOf(s)).get(0).getItemStatus().equals(LENT))
                .count();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser the type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, LIBRARIAN)) {

            /*
             * GET THE ITEM ID SELECTED BY THE USER IN THE BROWSER
             */
            String itemIdInfo = request.getParameter("itemIdLent");
            String customerInfo = request.getParameter("customerIdLent");
            String startDate = String.valueOf(request.getParameter("startDateLent"));
            String dueDate = String.valueOf(request.getParameter("dueDateLent"));

            ItemDAO itemDAO = new ItemDAO();
            /*
             * ITEM table options (select by):
             * -1 = no option
             * 1 = item id
             * 2 = item title
             * 3 = item author
             * 4 = item type
             * 5 = item status
             */
            ItemDTO itemDTO = itemDAO.get(ID, itemIdInfo).get(0);

            /*
             * - If the item to be lent is available AND
             * - The customer which will lent an item which is already registered
             */
            if (itemDTO.getItemStatus().equals(AVAILABLE)) {

                if (!customerInfo.isEmpty()) {

                    if (!startDate.isEmpty()) {

                        if (!dueDate.isEmpty()) {

                            HistoryDAO historyDAO = new HistoryDAO();

                            /*
                             * There should be a maximum limit MAX_BOOKS_ISSUED_TO_A_USER (5) on how many books a member can lend.
                             * - get all the items lent by the current customer
                             * - for the customer if of history get the item id from history
                             * - each item id obtained from history will be checked to see if its item status is lent
                             * - count all these obtained items (which are lent by the current user)
                             */
                            long itemsNumberLentByACustomer = getOneCustomerLentItems(historyDAO, itemDAO, customerInfo);
                            if (itemsNumberLentByACustomer <= MAX_BOOKS_ISSUED_TO_A_USER) {

                                /*
                                 * FILL IN HISTORY TABLE ALL THE LENT ITEMS
                                 *
                                 * new Date(0) = 1970-01-01 => used to specify the item is lent with no return date set
                                 * new Date(0) = 1969-12-31 => used to specify the item is lent with no return date set
                                 */
                                historyDAO.insert(new HistoryDTO(
                                        Integer.valueOf(itemIdInfo),
                                        Integer.valueOf(customerInfo),
                                        getSqlDate(startDate),
                                        getSqlDate(dueDate),
                                        new Date(0)));

                                /*
                                 * UPDATE THE ITEM TABLE WITH THE ITEM STATUS LENT
                                 * If the item is LENT the return date is the due date
                                 */
                                itemDTO.setItemStatus(LENT);
                                itemDAO.update(itemDTO);
                                // the information is correct and introduced into the database ==> current servlet is calling the AdminHome servlet
                                out.print("<p><strong><mark> !!!!!!!!! " + itemDTO +
                                        "<br> !!!!!!!!! Has been lent. Due date is " + dueDate + ". " +
                                        "Number of lent items is " + itemsNumberLentByACustomer + "</mark></strong></p>");
                            } else {
                                // the information is NOT correct and introduced into the database ==> current servlet is calling the AdminHome servlet
                                out.print("<p><strong><mark>!!!!!!!!! The user has already lent the maximum number of books " +
                                        MAX_BOOKS_ISSUED_TO_A_USER + "</mark></strong></p>");
                            }

                        } else {
                            // the information is NOT correct and introduced into the database ==> current servlet is calling the AdminHome servlet
                            out.print("<p><strong><mark>!!!!!!!!! Please select a due date.</mark></strong></p>");
                        }
                    } else {
                        // the information is NOT correct and introduced into the database ==> current servlet is calling the AdminHome servlet
                        out.print("<p><strong><mark>!!!!!!!!! Please select a start date.</mark></strong></p>");
                    }
                } else {
                    // the information is NOT correct and introduced into the database ==> current servlet is calling the AdminHome servlet
                    out.print("<p><strong><mark>!!!!!!!!! There is no customer registered.</mark></strong></p>");
                }
            } else {
                // the information is NOT correct and introduced into the database ==> current servlet is calling the AdminHome servlet
                out.print("<p><strong><mark>!!!!!!!!! The item " + itemDTO +
                        "!!!!!!!!! is not AVAILABLE.</mark></strong></p>");
            }
            request.getRequestDispatcher("ItemHomeServlet").include(request, response);
        } else {
            out.print("!!!! Sorry. Administrator already logged in. Please try again");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}
