package teme.project.item;

import teme.project.general.DBConstants;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static teme.project.general.DBConstants.UserType.LIBRARIAN;

/*
This Servlet shall update the database with the librarian data written by the user in the browser
(administrator has rights to introduce a librarian)
 */

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */

@WebServlet("/LibrarianHomeServlet")
public class LibrarianHomeServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * Print in the browser the links to the following views:
     * - LOGOUT VIEW
     * - ITEM LIBRARY VIEW
     * - CUSTOMER VIEW
     */
    private static void browseLogoutItemCustomerViews(PrintWriter out, HttpSession session) {

        out.print(UiUtil.servletSessionInfo(session) +
                "<h2>Librarian View</h2>" +
                "<a href='LogoutUserServlet' class='btn btn-primary' role='button'>Logout</a><br><br>" +

                "<a href='ItemHomeServlet' class='btn btn-primary' role='button'>Library Items View</a><br><br>" +

                "<a href='CustomerHomeServlet' class='btn btn-primary' role='button'>Customers View</a><br><br>");
    }

    /**
     * INFO VIEW
     */
    private static void browseLibrarianInfoView(PrintWriter out) {
        out.print("<br><br><p><strong>Note:</strong>" +
                "<li>The librarian users cannot manage the contents of the library.</li>" +
                "<li>The librarian users cannot update the users of the system..</li></p>" +
                "<p><strong>Librarian users are allowed to perform these item-related operations :</strong>" +
                "<li>Add new items to the library.</li>" +
                "<li>View and search the contents of the library.</li>" +
                "<li>Update details of existing items (author name, description, etc).</li>" +
                "<li>Mark an item as lent with a set return date.</li>" +
                "<li>Mark an item as available when it is returned.</li>" +
                "<li>For each item in the library keep a historical record of when it was lent, for how many days and to what person.</li>" +
                "<li>Retire an item from the library (never delete it), by marking it as: lost, destroyed or unavailable.</li></p>");
    }

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        /*
        Tells to the browser hte type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();
        if (UiUtil.checkServletSession(session, LIBRARIAN)) {

            browseLogoutItemCustomerViews(out, session);


            browseLibrarianInfoView(out);
        } else {
            out.print("!!!! Sorry. Administrator already logged in. Please try again");
            request.getRequestDispatcher("index.html").include(request, response);
        }

    }
}
