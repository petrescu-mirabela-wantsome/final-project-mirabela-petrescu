package teme.project.item;

import teme.project.general.DBConstants;
import teme.project.general.UiUtil;
import teme.project.history.HistoryDAO;
import teme.project.history.HistoryDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static teme.project.general.DBConstants.DATE_MIN_01;
import static teme.project.general.DBConstants.DATE_MIN_02;
import static teme.project.general.DBConstants.HistoryField.ITEM_ID;
import static teme.project.general.DBConstants.ItemField.ID;
import static teme.project.general.DBConstants.ItemStatus.AVAILABLE;
import static teme.project.general.DBConstants.ItemStatus.LENT;
import static teme.project.general.DateFormatter.getSqlDate;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/ReturnItemServlet")
public class ReturnItemServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser the type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, DBConstants.UserType.LIBRARIAN)) {

            // get the item id selected by the user in the browser
            String itemInfo = request.getParameter("itemIdReturn");
            String returnDate = String.valueOf(request.getParameter("returnDateReturn"));

            /*
             * UPDATE THE ITEM TABLE WITH THE ITEM STATUS AVAILABLE
             * If the item is LENT the return date is the due date (from lent process)
             */
            ItemDAO itemDAO = new ItemDAO();

            /*
             * ITEM table options (select by):
             * -1 = no option
             * 1 = item id
             * 2 = item title
             * 3 = item author
             * 4 = item type
             * 5 = item status
             */
            ItemDTO itemDTO = itemDAO.get(ID, itemInfo).get(0);

            if (itemDTO.getItemStatus().equals(LENT)) {

                if (!returnDate.isEmpty()) {

                    /*
                     * FILL IN HISTORY TABLE THE END DATE COLUMN
                     */
                    HistoryDAO historyDAO = new HistoryDAO();

                    /*
                     * HISTORY table options (select by):
                     * -1 = no option
                     * 1 = item id
                     * 2 = customer id
                     * 3 = lent start date
                     * 4 = lent due date
                     * 5 = return date
                     */
                    List<HistoryDTO> historyDTOList = historyDAO.get(ITEM_ID, itemInfo);

                    for (HistoryDTO historyDTO : historyDTOList) {

                        // new Date(0) = 1970-01-01 used to specify that the item was lent and the return date is not set
                        // new Date(0) = 1969-12-31 used to specify that the item was lent and the return date is not set
                        if (historyDTO.getReturnDate().getTime() == DATE_MIN_01 ||
                                historyDTO.getReturnDate().getTime() == DATE_MIN_02) {
                            /*
                             * Update the end date in the history table (end date is the actual return date, not the planned one)
                             */
                            historyDTO.setReturnDate(getSqlDate(returnDate));
                            historyDAO.update(historyDTO);

                            /*
                             * UPDATE THE ITEM TABLE WITH THE ITEM STATUS LENT
                             * If the item is LENT the return date is the planned return date
                             * If the item is AVAILABLE, the return date is the actual return date
                             */
                            itemDTO.setItemStatus(AVAILABLE);
                            itemDAO.update(itemDTO);

                            out.print("<p><strong><mark> !!!!!!!!! " + itemDTO +
                                    "<br> !!!!!!!!! Has been returned. Return date is " + returnDate + "</mark></strong></p>");
                        }
                    }
                    // the information is correct and introduced into the database ==> current servlet is calling the AdminHome servlet
                } else {
                    // the information is NOT correct and introduced into the database ==> current servlet is calling the AdminHome servlet
                    out.print("<p><strong><mark>!!!!!!!!! Please introduce a return date.</mark></strong></p>");
                }

            } else {
                // the information is NOT correct and introduced into the database ==> current servlet is calling the AdminHome servlet
                out.print("<p><strong><mark>!!!!!!!!! The item " + itemDTO + "" +
                        "!!!!!!!!! Is not recorded as LENT.</mark></strong></p>");
            }
            request.getRequestDispatcher("ItemHomeServlet").include(request, response);
        } else {
            out.print("!!!! Sorry. Administrator already logged in. Please try again");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}
