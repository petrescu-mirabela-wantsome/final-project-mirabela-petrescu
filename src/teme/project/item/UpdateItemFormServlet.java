package teme.project.item;

import teme.project.general.DBConstants;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static teme.project.general.DBConstants.UserType.LIBRARIAN;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/UpdateItemFormServlet")
public class UpdateItemFormServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */


    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser the type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        /*
         * // http://tutorials.jenkov.com/java-servlets/httpsession.html
         * The HttpSession object represents a user session.
         * A user session contains information about the user across multiple HTTP requests.
         */
        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, LIBRARIAN)) {

            String id = request.getParameter("id");   // get the id value to be deleted selected from the browser

            ItemDAO itemDAO = new ItemDAO();

            /*
             * option (select by):
             * -1 = no option
             * 1 = item id
             * 2 = item title
             * 3 = item author
             * 4 = item type
             * 5 = item status
             */
            ItemDTO itemDTO = itemDAO.get(DBConstants.ItemField.ID, id).get(0);

            /*
             * View Title and BACK button (Item View)
             */
            out.println("<!DOCTYPE html><html><head><title>Edit Item Library Form</title></head><body>" +
                    UiUtil.servletSessionInfo(session) +
                    "<br><br><a href='ItemComparatorTableServlet' class='btn btn-primary' role='button'>Back (Item View Home)</a><br><br>");

            /*
             * ADD INTO A TABLE IN THE BROWSER THE SELECTED ITEM CONTENT
             * The id is taken from the browser when button delete is pressed
             */
            out.print("<form action='UpdateItemServlet' method='post' style='width:300px'>" +
                    "<table class='table table-bordered table-striped'>" +
                    "<tr><td><label for='id1'>Item ID</label></td>" +
                    "<td><input type='text' class='form-control' value='" + itemDTO.getId() + "' name='id' id='id1' placeholder='Item ID' readonly/></td>" +
                    "</tr>" +

                    "<tr><td><label for='title1'>Title</label></td>" +
                    "<td><input type='text' class='form-control' value='" + itemDTO.getTitle() + "' name='title' id='title1' placeholder='Title'/></td>" +
                    "</tr>" +

                    "<tr><td><label for='author1'>Author</label></td>" +
                    "<td><input type='text' class='form-control' value='" + itemDTO.getAuthor() + "' name='author' id='author1' placeholder='Author'/></td>" +
                    "</tr>");

            /*
             * CREATE A DROP DOWN LIST OF THE ITEM TYPE (<option></>)
             * The item information is taken from the database
             */
            out.println("<tr><td><label for='itemtype1'>Item Type</label>" +
                    "<td><select name=\"itemtype\">");
            UiUtil.createEnumDropDownList(out, DBConstants.ItemType.values(), itemDTO.getItemType());

            /*
             * CREATE A DROP DOWN LIST OF THE ITEM STATUS (<option></>)
             * The item information is taken from the database
             */
            out.print("<tr><td><label for='itemstatus1'>Item Status</label>" +
                    "<td><select name=\"itemstatus\">");
            UiUtil.createEnumDropDownList(out, DBConstants.ItemStatus.values(), itemDTO.getItemStatus());

            out.print("</table><button type='submit' class='btn btn-primary'>Update</button>" +
                    "<button type='reset' class='btn btn-primary'>Reset</button>" +
                    "</form></body>");

        } else {
            out.print("!!!! Sorry. Administrator already logged in. Please try again");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}
