package teme.project.item;

import teme.project.general.DBConstants;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static teme.project.general.DBConstants.ItemField.ID;
import static teme.project.general.DBConstants.ItemStatus.LENT;
import static teme.project.general.DBConstants.UserType.LIBRARIAN;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/UpdateItemServlet")
public class UpdateItemServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser the type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        HttpSession session = request.getSession();

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        if (UiUtil.checkServletSession(session, LIBRARIAN)) {

            String id = request.getParameter("id");   // get the id value to be deleted selected from the browser
            String title = request.getParameter("title");
            String author = request.getParameter("author");
            String itemType = request.getParameter("itemtype");
            String itemStatus = request.getParameter("itemstatus");

            if (UiUtil.isEmptyString(title) &&
                    UiUtil.isEmptyString(author) &&
                    UiUtil.isEmptyString(itemType) &&
                    UiUtil.isEmptyString(itemStatus)) {

                ItemDAO itemDAO = new ItemDAO();

                ItemDTO itemDTO = itemDAO.get(ID, id).get(0);

                if (!itemDTO.getItemStatus().equals(LENT)) {

                    itemDTO.setTitle(title);
                    itemDTO.setAuthor(author);
                    itemDTO.setItemType(DBConstants.ItemType.valueOf(itemType));
                    itemDTO.setItemStatus(DBConstants.ItemStatus.valueOf(itemStatus));
                    itemDAO.update(itemDTO);

                /*
                The RequestDispatcher class enables your servlet to "call" another servlet from inside another servlet.
                The other servlet is called as if an HTTP request was sent to it by a browser.
                Link: http://tutorials.jenkov.com/java-servlets/requestdispatcher.html
                */
                    // the current servlet is not changed, because there is no information in the database
                    out.print("<p><strong><mark> !!!!!!!!! " + itemDTO +
                            "<br> !!!!!!!!! Successfully Updated.</mark></strong></p>");
                    request.getRequestDispatcher("ItemComparatorTableServlet").include(request, response);
                } else {
                    // the current item is already lent - can not update the item fields
                    out.print("<p><strong><mark> !!!!!!!!! Item lent, so it can not be updated .</mark></strong></p>");
                    request.getRequestDispatcher("ItemComparatorTableServlet").include(request, response);
                }
            } else {
                // the current servlet is calling UpdateItemFormServlet (because the information is not filled in correctly) ==> NOT introduced into the data base
                // the user has to introduce again the information into the browser
                out.print("<br><p><strong><mark>!!!!!! Please try again. The user properties are not filled correctly </p></strong></mark>");
                out.println("<td><a href='UpdateItemFormServlet?id=" + id + "'>Try again to introduce</a></td>");
            }
        } else {
            out.print("</p></strong></mark>!!!! Sorry. Administrator already logged in. Please try again.</p></strong></mark>");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}
