package teme.project.user;

import teme.project.general.DBConstants;
import teme.project.general.ReadPropertiesFile;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static teme.project.general.DBConstants.UserField.EMAIL;
import static teme.project.general.DBConstants.UserField.USERNAME;
import static teme.project.general.DBConstants.UserType.ADMIN;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/AddUserCsvServlet")   // THE SERVLET SHALL BE DELETED - IT IS NOT USED

/*
 * The @MultipartConfig annotation indicates that the servlet expects requests to made using the multipart/form-data MIME type.
 * LINK: https://docs.oracle.com/javaee/6/tutorial/doc/glraq.html
 */
@MultipartConfig
public class AddUserCsvServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * Read the user information from csv file from the uploaded_files folder (uploaded in the browser)
     */
    private static List<UserDTO> readUserFromCsv(String path) throws FileNotFoundException {
        List<UserDTO> userDTOList = new ArrayList<>();
        File csvFile = new File(path);
        Scanner fileScanner = new Scanner(csvFile);
        while (fileScanner.hasNext()) {
            String line = fileScanner.nextLine();
            String[] csvLineArray = line.split(",");
            try {
                userDTOList.add(new UserDTO(csvLineArray[0], csvLineArray[1], csvLineArray[2], csvLineArray[3], DBConstants.UserType.valueOf(csvLineArray[4])));
            } catch (NumberFormatException e1) {
                System.out.println("Wrong type => " + Arrays.toString(csvLineArray));
            } catch (ArrayIndexOutOfBoundsException e2) {
                System.out.println("Less fields than expected => " + Arrays.toString(csvLineArray));
            } catch (IllegalArgumentException e3) {
                System.out.println("Wrong type => " + Arrays.toString(csvLineArray));
            }
        }
        return userDTOList;
    }

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser hte type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        /*
         * Link: http://tutorials.jenkov.com/java-servlets/httpsession.html
         * The HttpSession object represents a user session.
         * A user session contains information about the user across multiple HTTP requests.
         */
        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, ADMIN)) {

            // list of users from csv file = readUserFromCsv(ReadPropertiesFile.readUserCsvPath(ReadPropertiesFile.loadConfigFromFile()))

            UserDAO userDAO = new UserDAO();
            for (UserDTO userDTO : readUserFromCsv(ReadPropertiesFile.readUserCsvPath(ReadPropertiesFile.loadConfigFromFile()))) {
                /*
                 * if the new user is not in database
                 * username and email are unique in database
                 */
                if (userDAO.get(USERNAME, userDTO.getUsername()).isEmpty() ||
                        userDAO.get(EMAIL, userDTO.getEmail()).isEmpty() ||
                        userDAO.get(userDTO)) {
                    out.println("<p><strong><mark>!!!!!! User " + userDTO +
                            "<br>!!!!!! Successfully introduced in the database" + "</p></strong></mark>");
                    userDAO.insert(userDTO);      // write into user table of the data base the info written by the user in the browser
                }
                /*
                 * if the new user read from csv file already exist in database
                 */
                else {
                    out.println("<p><strong><mark>!!!!! Duplicate entry. User already in database ==> " + userDTO + "</p></strong></mark>");
                }
            }

            /*
            The RequestDispatcher class enables your servlet to "call" another servlet from inside another servlet.
            The other servlet is called as if an HTTP request was sent to it by a browser.
            Link: http://tutorials.jenkov.com/java-servlets/requestdispatcher.html
            */

            // the information is correct and introduced into the database ==> current servlet is calling the AdminHome servlet
            request.getRequestDispatcher("AdminHomeServlet").include(request, response);
        } else {
            out.print("!!!! Sorry. User already logged in. Please try again");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}
