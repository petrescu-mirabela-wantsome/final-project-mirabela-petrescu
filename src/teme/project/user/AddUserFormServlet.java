package teme.project.user;

import teme.project.general.DBConstants;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static teme.project.general.DBConstants.UserType.ADMIN;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/AddUserFormServlet")
public class AddUserFormServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */


    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser the type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, ADMIN)) {

            out.print("<!DOCTYPE html><html>" +
                    "<head><title>Edit User Form</title></head>" +
                    UiUtil.servletSessionInfo(session) +
                    "<body><br><br><a href='LogoutUserServlet' class='btn btn-primary' role='button'>Logout</a>" +
                    "<br><br><a href='AdminHomeServlet' class='btn btn-primary' role='button'>Back (Administrator Home view)</a></ul><br><br>");

            out.print("<form action='AddUserServlet' method='post' style='width:300px'>" +
                    "<table class='table table-bordered table-striped'>" +

                    "<tr><td><label for=\"username1\">User Name Account</label></td>" +
                    "<td><input type=\"text\" class=\"form-control\" name=\"username\" id=\"username1\" placeholder=\"User Account\"/>" +
                    "</td></tr>" +

                    "<tr><td><label for=\"password1\">Password</label></td>" +
                    "<td><input type=\"password\" class=\"form-control\" name=\"password\" id=\"password1\" placeholder=\"Password\"/>" +
                    "</td></tr>" +

                    "<tr><td><label for=\"realname1\">Real user name</label></td>" +
                    "<td><input type=\"text\" class=\"form-control\" name=\"realname\" id=\"realname1\"/>" +
                    "</td></tr>" +

                    "<tr><td><label for=\"email1\">Email address</label></td>" +
                    "<td><input type=\"email\" class=\"form-control\" name=\"email\" id=\"email1\"/>" +
                    "</td></tr>" +

                    "<tr><td><label for='usertype1'>User type</label>" +
                    "<td><select name=\"usertype\">");
            UiUtil.createEnumDropDownList(out, DBConstants.UserType.values(), DBConstants.UserType.values()[0]);

            out.print("</table><br><button type='submit' class='btn btn-primary'>Add</button>" +
                    "<button type='reset' class='btn btn-primary'>Reset</button>" +
                    "</form></body>");

        } else {
            out.print("!!!! Sorry. User already logged in. Please try again");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}
