package teme.project.user;

import teme.project.general.DBConstants;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static teme.project.general.DBConstants.UserField.EMAIL;
import static teme.project.general.DBConstants.UserField.USERNAME;
import static teme.project.general.DBConstants.UserType.ADMIN;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/AddUserServlet")
public class AddUserServlet extends HttpServlet {

    /*
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /*
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser hte type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        // get the new admin info written by the user in the browser
        String userName = request.getParameter("username");
        String password = request.getParameter("password");
        String realName = request.getParameter("realname");
        String email = request.getParameter("email");
        String userType = request.getParameter("usertype");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        /*
         * Link: http://tutorials.jenkov.com/java-servlets/httpsession.html
         * The HttpSession object represents a user session.
         * A user session contains information about the user across multiple HTTP requests.
         */
        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, ADMIN)) {

            if (UiUtil.isEmptyString(userName) &&
                    UiUtil.isEmptyString(realName) &&
                    UiUtil.isEmptyString(password) &&
                    UiUtil.isEmptyString(email)) {

                UserDAO userDAO = new UserDAO();
                UserDTO userDTO = new UserDTO(userName, password, realName, email, DBConstants.UserType.valueOf(userType));

                /*
                 * if the new user is not in database
                 * username and email shall be unique
                 */
                if (userDAO.get(USERNAME, userName).isEmpty() &&
                        userDAO.get(EMAIL, email).isEmpty()) {
                    userDAO.insert(userDTO);      // write into user table of the data base the info written by the user in the browser

                    out.print("<p><strong><mark>!!!!!! User " + userDTO +
                            "<br>!!!!!! Successfully introduced in the database" + "</p></strong></mark>");
                } else {
                    out.println("<p><strong><mark>!!!!! Duplicate entry. User already in database ==> " + userDTO + "</p></strong></mark>");
                }
                /*
                The RequestDispatcher class enables your servlet to "call" another servlet from inside another servlet.
                The other servlet is called as if an HTTP request was sent to it by a browser.
                Link: http://tutorials.jenkov.com/java-servlets/requestdispatcher.html
                */
                // the information is correct and introduced into the database ==> current servlet is calling the AdminHome servlet
                request.getRequestDispatcher("AdminHomeServlet").include(request, response);
            } else {
                // the current servlet is calling AddUserFormServlet (because the information is not filled in correctly) ==> NOT introduced into the data base
                // the user has to introduce again the information into the browser
                out.print("<br><p><strong><mark>!!!!!! Please try again. The user properties are not filled correctly </p></strong></mark>" +
                        "   <br><br><a href='AddUserFormServlet' class='btn btn-primary' role='button'>Try again to introduce</a>");

                request.getRequestDispatcher("AddUserFormServlet").include(request, response);
            }
        } else {
            out.print("</p></strong></mark>!!!! Sorry. User already logged in. Please try again.</p></strong></mark>");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}
