package teme.project.user;

import teme.project.general.DBConstants;
import teme.project.general.ReadPropertiesFile;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static teme.project.general.DBConstants.UserType.ADMIN;


/*
This Servlet shall update the database with the librarian data written by the user in the browser
(administrator has rights to introduce a librarian)
 */

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */

@WebServlet("/AdminHomeServlet")
public class AdminHomeServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * ADD USER MANUALLY AND FROM CSV FILE
     */
    private static void addUserManuallyAndFromCsv(PrintWriter out, String path) {
        out.println("<a href='AddUserFormServlet' class='btn btn-primary' role='button'>Add User</a><br><br>");

        out.print("Select a file to upload (.csv format)" +
                "        <form method=\"POST\" action=\"FileUploadServlet\" enctype=\"multipart/form-data\" >" +
                "            File:" +
                "            <input type=\"file\" name=\"file\" id=\"file\" /> <br/>" +
                "            Destination:" +
                "            <input type=\"text\" value=\"" + path + "\" name=\"destination\"/>" +
                "            <input type=\"submit\" value=\"Upload\" name=\"upload\" id=\"upload\" />" +
                "        </form>");


//        /**
//         * ADD USER FROM CSV FILE
//         */
//        out.print("<a href='AddUserCsvServlet' class='btn btn-primary' role='button'>Add Users from CSV</a><br><br>");
    }

    /**
     * SEARCH USER BY NAME
     * Link: https://www.w3schools.com/howto/howto_js_search_menu.asp
     * Link: https://www.w3schools.com/howto/howto_js_filter_lists.asp
     * Link: https://www.w3schools.com/howto/howto_js_filter_table.asp
     */
    private static void searchUserByName(PrintWriter out) {
        // Link: https://www.quora.com/How-do-I-get-a-value-from-a-drop-down-list-in-JSP
        out.println("<br><strong><mark>Search User by name</strong></mark><form action=\"SearchUserServlet\" method=\"POST\">");

        out.print("<table class='table table-bordered table-striped'>" +
                "<tr><td><label for=\"username1\">User Name Account</label></td>" +
                "<td><input type=\"text\" class=\"form-control\" name=\"username\" id=\"username1\" placeholder=\"User Account\"/>" +
                "</td></tr></table>");

        out.print("<button type='submit' class='btn btn-primary'>Search</button>" +
                "<button type='reset' class='btn btn-primary'>Reset</button><br><br>" +
                "</form>");
    }

    /**
     * EXPORT USER INFORMATION FROM THE DATABASE INTO A CSV FILE
     * LINK: http://www.tutorialspoint.com/servlets/servlets-file-uploading.htm
     * https://www.baeldung.com/upload-file-servlet
     */
    private static void exportUserFromDatabase(PrintWriter out, String path) {
        out.print("<br><strong><mark>Export Users from Database (to csv file)</strong></mark><br>" +
                "        <form method=\"POST\" action=\"UserExportServlet\" enctype=\"multipart/form-data\" >" +
//                "            File:" +
//                "            <input type=\"file\" name=\"exportfile\" id=\"exportfile\" /> <br/>" +
                "            File exported in path:" +
                "            <input type=\"text\" value=\"" + path + "\" name=\"exportdestination\"/>" +
                "            <input type=\"submit\" value=\"Export\" name=\"upload\" id=\"upload\" />" +
                "        </form>");
    }

    /**
     * USER TABLE - SORTED BY THE OPTIONS SELECTED IN THE BROWSER
     */
    private static void userViewComparator(PrintWriter out) {
        out.print("<br><strong><mark>View Users (database information)</strong></mark>");

        // LINK: https://www.codejava.net/java-ee/servlet/handling-html-form-data-with-java-servlet
        // LINK: https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_select_multiple
        // LINK: https://ibytecode.com/blog/getting-checkbox-values-from-html-form-in-servlet/

        // report - will contain all the selected checkboxes from the browser => it is an array
        out.print(" <form action='UserComparatorTableServlet' method='post' style='width:300px'>" +
                "   Sort the users<br>" +
                "   <input type=\"checkbox\" name=\"comparator\" value=\"userCompByType\" > Sort the users by type<br>" +
                "   <input type=\"checkbox\" name=\"comparator\" value=\"userCompByUserName\" > Sort the users by user name<br>" +
                "   <input type=\"checkbox\" name=\"comparator\" value=\"userCompByRealName\" > Sort the users by real name<br>" +
                "   <button type=\"submit\" class=\"btn btn-primary\">View Library Users</button>" +
                "   <button type='reset' class='btn btn-primary'>Reset the sort</button></form></body>");
    }

    /**
     * ADMINISTRATOR INFO VIEW
     */
    private static void browseAdminInfoView(PrintWriter out) {
        out.print("<br><br><p><strong>Note:</strong>" +
                "<li>An Administrator User has the authority to manage the users of the library.</li>" +
                "<li>The administrator user can not add or update the items of the library.</li></p>" +
                "<p><strong>Administrator users are allowed to perform these user-related operations :</strong>" +
                "<li>Define new administrator or librarian users.</li>" +
                "<li>View existing users in the system (support also searching).</li>" +
                "<li>Update details of existing users (password, email etc)</li>" +
                "<li>Define new administrator or librarian users.</li>" +
                "<li>Delete administrator or librarian users. However, the last remaining administrator user cannot be deleted.</li></p>");
    }

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        /*
        Tells to the browser hte type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();
        if (UiUtil.checkServletSession(session, ADMIN)) {

        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

            String exportPath = ReadPropertiesFile.readUploadedFilesPath(ReadPropertiesFile.loadConfigFromFile());

            out.print(UiUtil.servletSessionInfo(session) +
                    "<h1>Administrator View</h1>" +
                    "<a href='LogoutUserServlet' class='btn btn-primary' role='button'>Logout</a><br><br>");

            addUserManuallyAndFromCsv(out, exportPath);


            searchUserByName(out);


            exportUserFromDatabase(out, exportPath);


            userViewComparator(out);

            browseAdminInfoView(out);

        } else {
            out.print("<p><strong><mark> !!!!!!!!! Sorry. User already logged in. Please try again.</mark></strong></p>");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}