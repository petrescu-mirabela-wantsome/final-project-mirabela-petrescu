package teme.project.user;

import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static teme.project.general.DBConstants.UserField.ID;
import static teme.project.general.DBConstants.UserType.ADMIN;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/DeleteUserServlet")
public class DeleteUserServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser hte type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        String id = request.getParameter("id");   // get the id value to be deleted selected from the browser
        UserDAO userDAO = new UserDAO();
        UserDTO userDTO = userDAO.get(ID, id).get(0);

        if (UiUtil.checkServletSession(session, ADMIN)) {
            if (!String.valueOf(session.getAttribute("userNameSession")).equals(userDTO.getUsername())) {

//            PrintWriter out = response.getWriter();
//            response.setContentType("text/html");
//            out.println("<script type=\"text/javascript\">");
//            out.println("confirm('Do you want to delete the user ?');");
//            out.println("</script>");
                userDAO.delete(userDTO);      // delete the id from the database
                out.print("<p><strong><mark> !!!!!!!!! " +
                        userDTO + "<br> !!!!!!!!! Deleted from the database.</mark></strong></p>");
            } else {
                /*
                The RequestDispatcher class enables your servlet to "call" another servlet from inside another servlet.
                The other servlet is called as if an HTTP request was sent to it by a browser.
                Link: http://tutorials.jenkov.com/java-servlets/requestdispatcher.html
                */
                out.print("<p><strong><mark> !!!!!!!!! " +
                        userDTO +
                        "<br> !!!!!!!!! Can not be deleted since the selected user is logged in.</mark></strong></p>");
            }
            request.getRequestDispatcher("UserComparatorTableServlet").include(request, response);
        } else {
            out.print("<p><strong><mark> !!!!!!!!! Sorry. User already logged in. Please try again.</mark></strong></p>");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}
