package teme.project.user;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/LogoutUserServlet")
public class LogoutUserServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.println("<!DOCTYPE html><html><head><title>Logout User</title></head><body>");

        /*
        Link: http://tutorials.jenkov.com/java-servlets/requestdispatcher.html
        ==> The RequestDispatcher class enables your servlet to "call" another servlet from inside another servlet.
            The other servlet is called as if an HTTP request was sent to it by a browser.
        ==> The include() method merges the response written by the calling servlet, and the activated servlet.
            This way you can achieve "server side includes" using the include().
         */
        request.getRequestDispatcher("index.html").include(request, response);

        out.println("<h1>You are successfully logged out</h1>");

        /*
        Link: http://tutorials.jenkov.com/java-servlets/httpsession.html
        ==> The HttpSession object represents a user session. A user session contains information about the user across multiple HTTP requests.
        ==> When a user enters your site for the first time, the user is given a unique ID to identify his session by.
        invalidate - invalidates the session and it removes all attributes from the session object
         */
        request.getSession().invalidate();

        /*
        Normally, the web application server automatically closes the PrintWriter when the service method exits.
        But not all application servers will do that.
         */
        out.close();
    }

}
