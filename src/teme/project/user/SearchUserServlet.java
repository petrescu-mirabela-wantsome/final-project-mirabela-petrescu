package teme.project.user;

import teme.project.general.DBConstants;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static teme.project.general.DBConstants.UserField.USERNAME;
import static teme.project.general.DBConstants.UserType.ADMIN;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/SearchUserServlet")
public class SearchUserServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        process(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        process(request, response);
    }


    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser hte type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        // get the username selected by the user in the browser
        String searchUsername = request.getParameter("username");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, ADMIN)) {

            out.println(UiUtil.servletSessionInfo(session) +
                    "<br><h2>Search User View (database information)</h2><br>");

            // logout and back button
            out.print("<a href='LogoutUserServlet' class='btn btn-primary' role='button'>Logout</a><br>" +
                    "<br><a href='AdminHomeServlet' class='btn btn-primary' role='button'>Administrator Home</a><br>");

            /*
             * CREATE INTO THE BROWSER THE USER TABLE, IMPORTED FROM THE DATABASE
             * It will contain only the item user which has the name searchUsername, selected in the browser
             * https://www.w3schools.com/howto/howto_js_filter_table.asp
             */
            UiUtil.userViewTable(out, new UserDAO().get(USERNAME, searchUsername));

        } else {
            out.print("!!!! Sorry. User already logged in. Please try again");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}
