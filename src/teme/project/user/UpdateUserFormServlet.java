package teme.project.user;

import teme.project.general.DBConstants;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static teme.project.general.DBConstants.UserField.ID;
import static teme.project.general.DBConstants.UserType.ADMIN;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/UpdateUserFormServlet")
public class UpdateUserFormServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser the type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, ADMIN)) {

        /*
         Get the id value of the item which will be deleted (selected from the browser)
         */
            String id = request.getParameter("id");

            UserDTO userDTOItem = new UserDAO().get(ID, id).get(0);

            out.print("<!DOCTYPE html><html>" +
                    "<head><title>Edit User Form</title></head>" +
                    UiUtil.servletSessionInfo(session) +

                    "<body><br><br><a href='LogoutUserServlet' class='btn btn-primary' role='button'>Logout</a><br><br>" +
                    "<a href='UserComparatorTableServlet' class='btn btn-primary' role='button'>Back (User Table view)</a><br><br>" +

                    "<form action='UpdateUserServlet' method='post' style='width:300px'>" +
                    "<table class='table table-bordered table-striped'>" +
                    "<tr><td><label for='id1'>User ID</label></td>" +
                    "<td><input type='text' class='form-control' value='" + userDTOItem.getId() + "' name='id' id='id1' placeholder='User ID'/ readonly>" +
                    "</td></tr>" +

                    "<tr><td><label for='name1'>Name</label></td>" +
                    "<td><input type='text' class='form-control' value='" + userDTOItem.getUsername() + "' name='username' id='username1' placeholder='User Name'/>" +
                    "</td></tr>" +

                    "<tr><td><label for='password1'>Password</label></td>" +
                    "<td><input type='password' class='form-control' value='" + userDTOItem.getPassword() + "'  name='password' id='password1' placeholder='Password'/>" +
                    "</td></tr>" +

                    "<tr><td><label for='realname1'>Real name</label></td>" +
                    "<td><input type='text' class='form-control' value='" + userDTOItem.getRealName() + "'  name='realname' id='realname1' placeholder='Real name'/>" +
                    "</td></tr>" +

                    "<tr><td><label for='email1'>Email address</label></td>" +
                    "<td><input type='email' class='form-control' value='" + userDTOItem.getEmail() + "'  name='email' id='email1' placeholder='Email'/>" +
                    "</td></tr>" +

                    /*
                     * CREATE A DROP DOWN LIST OF THE USER TYPE (<option></>)
                     * The item information is taken from the database
                     */
                    "<tr><td><label for='usertype1'>User Type</label>" +
                    "<td><select name=\"usertype\">");
            UiUtil.createEnumDropDownList(out, DBConstants.UserType.values(), userDTOItem.getUserType());

            out.print("</table><br><button type='submit' class='btn btn-primary'>Update</button>" +
                    "   <button type='reset' class='btn btn-primary'>Reset</button>" +
                    "   </form></body>");

        } else {
            out.print("!!!! Sorry. User already logged in. Please try again");
            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}
