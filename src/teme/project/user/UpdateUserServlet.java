package teme.project.user;

import teme.project.general.DBConstants;
import teme.project.general.UiUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static teme.project.general.DBConstants.UserField.*;
import static teme.project.general.DBConstants.UserType.ADMIN;

/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/UpdateUserServlet")
public class UpdateUserServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser the type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        /*
         * // http://tutorials.jenkov.com/java-servlets/httpsession.html
         * The HttpSession object represents a user session.
         * A user session contains information about the user across multiple HTTP requests.
         */
        HttpSession session = request.getSession();

        if (UiUtil.checkServletSession(session, ADMIN)) {

            String id = request.getParameter("id");   // get the id value to be deleted selected from the browser
            String userName = request.getParameter("username");
            String password = request.getParameter("password");
            String realName = request.getParameter("realname");
            String email = request.getParameter("email");
            String userType = request.getParameter("usertype");

            if (UiUtil.isEmptyString(userName) &&
                    UiUtil.isEmptyString(password) &&
                    UiUtil.isEmptyString(realName) &&
                    UiUtil.isEmptyString(email)) {

                UserDAO userDAO = new UserDAO();

                /*
                 * USER table options (select by):
                 * -1 = no option
                 * 1 = user id
                 * 2 = user username
                 * 3 = password
                 * 4 = user real name
                 * 5 = user email
                 * 6 = user type
                 */
                UserDTO userDTOItem = userDAO.get(ID, id).get(0);
                userDTOItem.setUsername(userName);
                userDTOItem.setPassword(password);
                userDTOItem.setRealName(realName);
                userDTOItem.setEmail(email);
                userDTOItem.setUserType(DBConstants.UserType.valueOf(userType));

                /*
                 * if the updated user is not already present in data base, OR
                 * if the username is not already present in database
                 */
                if (userDAO.get(USERNAME, userName).stream().anyMatch(s -> s.getId() != userDTOItem.getId()) ||
                        userDAO.get(EMAIL, email).stream().anyMatch(s -> s.getId() != userDTOItem.getId())) {

                    userDAO.update(userDTOItem);

                    out.println("<p><strong><mark>!!!!! Duplicate entry. User already in database ==> " +
                            userDTOItem + "</p></strong></mark>");
                    // write into user table of the data base the info written by the user in the browser
                } else {
                    out.print("<p><strong><mark> !!!!!!!!! " + userDTOItem +
                            "<br> !!!!!!!!! Successfully Updated.</mark></strong></p>");
                }

                /*
                The RequestDispatcher class enables your servlet to "call" another servlet from inside another servlet.
                The other servlet is called as if an HTTP request was sent to it by a browser.
                Link: http://tutorials.jenkov.com/java-servlets/requestdispatcher.html
                */
                request.getRequestDispatcher("UserComparatorTableServlet").include(request, response);

            } else {
                // the current servlet is calling AddUserFormServlet (because the information is not filled in correctly) ==> NOT introduced into the data base
                // the user has to introduce again the information into the browser
                out.print("<br><p><strong><mark>!!!!!! Please try again. The user properties are not filled correctly </p></strong></mark>" +
                        "   <td><a href='UpdateUserFormServlet?id=" + id + "'>Try again to introduce</a></td>");
            }

        } else {
            out.print("</p></strong></mark>!!!! Sorry. User already logged in. Please try again.</p></strong></mark>");

            request.getRequestDispatcher("index.html").include(request, response);
        }
    }
}
