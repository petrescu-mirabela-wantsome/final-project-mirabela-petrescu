package teme.project.user;

import java.util.Comparator;

/*
LINK: https://howtodoinjava.com/sort/groupby-sort-multiple-comparators/
 */

class UserComparator {

    static class UserComparatorByUserName implements Comparator<UserDTO> {

        @Override
        public int compare(UserDTO o1, UserDTO o2) {
            return o1.getUsername().toLowerCase().compareTo(o2.getUsername().toLowerCase());
        }
    }


    static class UserComparatorByUserType implements Comparator<UserDTO> {

        @Override
        public int compare(UserDTO o1, UserDTO o2) {
            return String.valueOf(o1.getUserType()).toLowerCase().compareTo(String.valueOf(o2.getUserType()).toLowerCase());
        }
    }


    static class UserComparatorByRealName implements Comparator<UserDTO> {

        @Override
        public int compare(UserDTO o1, UserDTO o2) {
            return o1.getRealName().toLowerCase().compareTo(o2.getRealName().toLowerCase());
        }
    }

}
