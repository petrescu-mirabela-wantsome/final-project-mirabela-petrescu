package teme.project.user;

import teme.project.general.AbstractDao;
import teme.project.general.DBConstants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO extends AbstractDao<UserDTO, DBConstants.UserField> {

    //DB table related constants
    private static final String TABLE_NAME = "user";
    private static final String ID = "id";
    private static final String USER_NAME = "username";
    private static final String PASSWORD = "password";
    private static final String REAL_NAME = "realname";
    private static final String EMAIL = "email";
    private static final String USER_TYPE = "usertype";

    @Override
    protected List<UserDTO> executeSelect(Connection c) {
        String sql = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + ID;
        List<UserDTO> results = new ArrayList<>();
        try (PreparedStatement ps = c.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                extractUserFromResult(results, rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    private void extractUserFromResult(List<UserDTO> results, ResultSet rs) throws SQLException {
        results.add(new UserDTO(rs.getInt(ID), rs.getString(USER_NAME), rs.getString(PASSWORD), rs.getString(REAL_NAME),
                rs.getString(EMAIL), DBConstants.UserType.valueOf(rs.getString(USER_TYPE))));
    }


    @Override
    protected boolean executeSelect(Connection c, UserDTO userObject) {
        String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + USER_NAME + " = ? AND " +
                PASSWORD + " = ? AND " + REAL_NAME + " = ? AND " + EMAIL + " = ? AND " + USER_TYPE + " = ?";
        boolean status = false;
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setString(1, userObject.getUsername());
            ps.setString(2, userObject.getPassword());
            ps.setString(3, userObject.getRealName());
            ps.setString(4, userObject.getEmail());
            ps.setString(5, String.valueOf(userObject.getUserType()));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                status = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }


    @Override
    protected List<UserDTO> executeSelect(Connection c, DBConstants.UserField option, String userValue) {
        String sql = "";
        List<UserDTO> results = new ArrayList<>();
        switch (option) {
            case ID:
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = ?";
                break;
            case USERNAME:
                // LINK: https://alvinalexander.com/blog/post/jdbc/jdbc-preparedstatement-select-like
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE UPPER(" + USER_NAME + ") LIKE ?";
                // going to do a search using "upper" and add the specific criteria
                userValue = "%" + userValue.toUpperCase() + "%";
                break;
            case REAL_NAME:
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + REAL_NAME + " = ?";
                break;
            case EMAIL:
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + EMAIL + " = ?";
                break;
            case USER_TYPE:
                sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + USER_TYPE + " = ?";
                break;
        }
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setString(1, userValue);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                extractUserFromResult(results, rs);
            }
            return results;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void executeInsert(Connection c, UserDTO object) {
        String sql = "INSERT INTO " + TABLE_NAME +
                " (" + USER_NAME + ", " + PASSWORD + ", " + REAL_NAME + ", " + EMAIL + ", " + USER_TYPE + ") VALUES(?,?,?,?,?)";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setString(1, object.getUsername());
            ps.setString(2, object.getPassword());
            ps.setString(3, object.getRealName());
            ps.setString(4, object.getEmail());
            ps.setString(5, object.getUserType().name());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void executeUpdate(Connection c, UserDTO object) {
        String sql = "UPDATE " + TABLE_NAME +
                " SET " + USER_NAME + " = ?, " + PASSWORD + " = ?, " + REAL_NAME + " = ?, " +
                EMAIL + " = ?, " + USER_TYPE + " = ? WHERE " + ID + " = ?";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setString(1, object.getUsername());
            ps.setString(2, object.getPassword());
            ps.setString(3, object.getRealName());
            ps.setString(4, object.getEmail());
            ps.setString(5, object.getUserType().name());
            ps.setInt(6, object.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void executeDelete(Connection c, UserDTO object) {
        String sql = "DELETE FROM " + TABLE_NAME + " WHERE " + ID + " = ?";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setInt(1, object.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String authenticate(Connection c, String username, String password) {
        String status = "none";
        String sqlAdmin, sqlLibrarian;
        sqlAdmin = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + USER_NAME + " = ? AND " + PASSWORD + " = ? AND " + USER_TYPE + " = 'ADMIN'";
        try (PreparedStatement psAdmin = c.prepareStatement(sqlAdmin)) {
            psAdmin.setString(1, username);
            psAdmin.setString(2, password);

            ResultSet rsAdmin = psAdmin.executeQuery();
            if (rsAdmin.next()) {
                status = "admin";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        sqlLibrarian = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + USER_NAME + " = ? AND " + PASSWORD + " = ? AND " + USER_TYPE + " = 'LIBRARIAN'";
        try (PreparedStatement psLibrarian = c.prepareStatement(sqlLibrarian)) {
            psLibrarian.setString(1, username);
            psLibrarian.setString(2, password);

            ResultSet rsLibrarian = psLibrarian.executeQuery();
            if (rsLibrarian.next()) {
                status = "librarian";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return status;
    }
}
