package teme.project.user;

import teme.project.general.DBConstants;

import java.util.Objects;

public class UserDTO {

    private int id;
    private String username;
    private String password;
    private String realName;
    private String email;
    private DBConstants.UserType userType;

    public UserDTO() {
    }

    public UserDTO(String username, String password, String realName, String email, DBConstants.UserType userType) {
        this.username = username;
        this.password = password;
        this.realName = realName;
        this.email = email;
        this.userType = userType;
    }

    public UserDTO(int id, String username, String password, String realName, String email, DBConstants.UserType userType) {
        this(username, password, realName, email, userType);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public DBConstants.UserType getUserType() {
        return userType;
    }

    public void setUserType(DBConstants.UserType userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", realName='" + realName + '\'' +
                ", email='" + email + '\'' +
                ", userType=" + userType +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO userDTO = (UserDTO) o;
        return id == userDTO.id &&
                Objects.equals(username, userDTO.username) &&
                Objects.equals(password, userDTO.password) &&
                Objects.equals(realName, userDTO.realName) &&
                Objects.equals(email, userDTO.email) &&
                userType == userDTO.userType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, realName, email, userType);
    }
}


