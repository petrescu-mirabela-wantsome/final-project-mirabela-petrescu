package teme.project.user;

import teme.project.general.DBConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static teme.project.general.DBConstants.UserType.ADMIN;
import static teme.project.general.DBConstants.UserType.LIBRARIAN;


/*
The @WebServlet annotation is used for declaring a Servlet class
(i.e. the Servlet class must extend from the HttpServlet class) and configuring the mapping for it.
https://examples.javacodegeeks.com/enterprise-java/servlet/java-servlet-annotations-example/
 */
@WebServlet("/UserLoginServlet")
public class UserLoginServlet extends HttpServlet {

    /*
    The HttpServlet class reads the HTTP request, and determines if the request is an HTTP GET, POST, PUT, DELETE, HEAD etc.
    and calls one the corresponding method.
     */

    /**
     * @param request:  The purpose of the HttpRequest object is to represent the HTTP request a browser sends to your web application.
     *                  Thus, anything the browser may send, is accessible via the HttpRequest.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httprequest.html
     * @param response: The purpose of the HttpResponse object is to represent the HTTP response your web application sends back to the browser,
     *                  in response to the HTTP request the browser send to your web application.
     *                  Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        /*
         If the browser sends an HTTP POST request, the parameters are included in the body part of the HTTP request.
         */

        /*
        Tells to the browser hte type of the content you are sending back to it.
        For instance, the content type for HTML is text/html
         */
        response.setContentType("text/html");

        /*
        Writing HTML - To send HTML back to the browser, you have to obtain the a PrintWriter from the HttpResponse object.
        Link: http://tutorials.jenkov.com/java-servlets/httpresponse.html
         */
        PrintWriter out = response.getWriter();

        // Link: https://stackoverflow.com/questions/1577236/how-to-check-whether-a-user-is-logged-in-or-not-in-servlets
        // Link: https://www.quora.com/How-does-Servlet-JSP-know-if-the-user-is-logged-in-or-not-when-provided-with-a-link
        // Link: http://tutorials.jenkov.com/java-servlets/httpsession.html
        /*
        Session is a conversational state between client and server and it can consists of multiple request and response between client and server.
        Since HTTP and Web Server both are stateless, the only way to maintain a session is when some unique information
        about the session (session id) is passed between server and client in every request and response.
         */

        HttpSession session = request.getSession();

        //synchronized to prevent concurrent updates
        synchronized (session) {

            String userName = request.getParameter("username");
            String password = request.getParameter("password");

            String status = new UserDAO().get(userName, password);

            /*
             * IF IT IS A NEW SESSION (FIRST USER LOGGED IN ON THE SAME COMPUTER)
             * the algorithm is working if multiple user are trying to log in on the same browser
             */
            if (session.isNew()) {

                if (status.equals("admin")) {

                    session.setAttribute("userSession", ADMIN);
                    session.setAttribute("password", password);
                    session.setAttribute("userNameSession", userName);

                    out.println("WELCOME! YOU HAVE BEEN REGISTERED");

            /*
            The RequestDispatcher class enables your servlet to "call" another servlet from inside another servlet.
            The other servlet is called as if an HTTP request was sent to it by a browser.
            Link: http://tutorials.jenkov.com/java-servlets/requestdispatcher.html
             */
                    request.getRequestDispatcher("AdminHomeServlet").include(request, response);

                } else if (status.equals("librarian")) {

                    session.setAttribute("userSession", LIBRARIAN);
                    session.setAttribute("password", password);
                    session.setAttribute("userNameSession", userName);

                    out.println("WELCOME! YOU HAVE BEEN REGISTERED");

            /*
            The RequestDispatcher class enables your servlet to "call" another servlet from inside another servlet.
            The other servlet is called as if an HTTP request was sent to it by a browser.
            Link: http://tutorials.jenkov.com/java-servlets/requestdispatcher.html
             */
                    request.getRequestDispatcher("LibrarianHomeServlet").include(request, response);

                } else {
                    out.print("!!!! Sorry. Registration failed. Please try again");
                    request.getRequestDispatcher("index.html").include(request, response);
                }
            }
            /*
             * IF A SESSION IS ALREADY CREATED (NOT THE FIRST USER LOGGED IN ON THE SAME COMPUTER, SAME BROWSER)
             */
            else {
                out.print("<strong><mark>!!!! Sorry. Registration failed. A user already logged in. Redirection to the original login</mark></strong>");
                // if an admin logged in in the previous session => redirect to that page
                if (session.getAttribute("userSession").equals(ADMIN)) {
                    request.getRequestDispatcher("AdminHomeServlet").include(request, response);
                }
                // if an librarian logged in in the previous session => redirect to that page
                else if (session.getAttribute("userSession").equals(LIBRARIAN)) {
                    request.getRequestDispatcher("LibrarianHomeServlet").include(request, response);
                }
            }
        }
    }
}


